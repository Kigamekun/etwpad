<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use App\Datapokok;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['checksso'])->group(function () {

// Route::get('/logout', function () {
//     Auth::logout();

//     // return redirect;     
// });
Route::get('/', [UserController::class, 'index'])->name('user');
});


Route::get('/API_data_keluar', [UserController::class, 'API_data_keluar'])->name('API_data_keluar');
Route::get('/Senkeluar', [UserController::class, 'index'])->name('user');
Route::get('/cro', [UserController::class, 'index'])->name('user');
Route::get('/Senkeluar', [UserController::class, 'index'])->name('user');

// Route::get('/user', function () {
//     return view('user.user');
// });
