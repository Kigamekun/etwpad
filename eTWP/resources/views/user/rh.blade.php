<div class="mainContent">
    <div class="container-fluid">
        <div class="wrapperContent">
            <h1 class="nameContent">Data RH</h1>
            <div class="d-flex dataWrapper">
                <div class="imgProfil">
                    @if (!is_null(DB::connection('login')->table('users')->where('nrp', $data->nrp)->first()->thumb,))
                        <img src="{{ DB::connection('login')->table('users')->where('nrp', $data->nrp)->first()->thumb }}"
                            alt="" style="width: 200px;height: 200px;border-radius: 50%;min-width:200px;min-height:200px;max-width:200px;max-height:200px;" class="mt-2">
                    @else
                        <img src="https://www.pngkey.com/png/full/115-1150152_default-profile-picture-avatar-png-green.png"
                            alt="" style="min-width: 150px;max-width: 200px;" class="mt-2">
                    @endif
                </div>
                <div class=" wrapperTableInfoData d-flex">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>
                                <td>NRP</td>