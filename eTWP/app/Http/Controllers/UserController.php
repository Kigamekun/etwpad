<?php

namespace App\Http\Controllers;

use App\Models\Datapokok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Mutasi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    // select a.nrp, a.nama from sisfobeta_baltab.datapokok as a, sisfobeta_iuran_twp.users as b where a.nrp=b.nrp limit 100
    public function index()
    {
        // dd(Auth::user()->nrp);
        $data = DB::table('users')->where('nrp', Auth::user()->nrp)->first();
        $twp = Http::get('http://127.15.30.74/baltab/twp/'. Auth::user()->nrp);
        // dd($twp->json());
        // dd($data);
        $mutasi = DB::table('mutasi_history')->where('nrp', Auth::user()->nrp_new)->first();
        // dd($mutasi);
        if($mutasi == null){
            return view('user.user', [
                'data' => $data,
                'twp' => $twp
            ]);
        }
        if($data == null){
            return redirect('http://127.15.30.74/baltab/datapok?m=true');
        }
        // dd($datas);
        return view('user.user', [
            'data' => $data,
            'twp' => $twp,
            'mutasi' => $mutasi
        ]);


    }

    public function table()
    {

    }

    public function update(Request $req)
    {
        DB::table('datapokok')->where('nrp', $req->nrp)->update([
            "nrp" => $req->nrp,
            "nama" => $req->nama,
            "pangkat" => $req->pangkat,
            "tmt_abri" => $req->tmt_abri,
            "korp" => $req->korp,
            "satker" => $req->satker,
            "ku_ktm" => $req->ku_ktm,
            "tg_lahir" => $req->tg_lahir,
            "tmt_1" => $req->tmt_1,
            "tmt_2" => $req->tmt_2,
            "tmt_3" => $req->tmt_3,
            "tmt_4" => $req->tmt_4,
            "tmt_5" => $req->tmt_5,
            "nm_kel1" => $req->nm_kel1,
            "nm_kel2" => $req->nm_kel2,
            "nm_kel3" => $req->nm_kel3,
            "alamat" => $req->alamat,
            "norek" => $req->norek,
            "nm_bank" => $req->nm_bank,
            "nama_rek" => $req->nrp
        ]);

        return view('user.edit');
    }


    public function cro()
    {
        $data=DB::table('users')->whereNotNull(['norek','narek','nrp'])->limit(5)->get();
        foreach ($data as $key => $dt) {
                DB::table('TRANSAKSI POTONGAN')->insert([
                        'Nama'=>$dt->narek,
                        'Nomor Rekening'=>$dt->norek,
                        'NRP'=>$dt->nrp,
                        'Besar Potongan'=>150000,
                        'tgl'=>'2021-10-01',
                        'date'=>'2021-10-01'
                ]);
        }
    }
    public function API_data_keluar()
    {
        // $this->cro();
        $data=DB::table('TRANSAKSI POTONGAN')->whereNotNull(['Nomor Rekening','Nama','NRP'])->where('tgl','2021-10-01')->get();
        $pin=[];
        foreach ($data as $key => $dt) {
            $pin[$key]=$dt;
        }
        return response()->json($pin, 200);
    }

    public function FunctionName($value='')
    {
        // code...
    }
}
