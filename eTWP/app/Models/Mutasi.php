<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mutasi extends Model
{
    use HasFactory;

    protected $table = 'mutasi_history';
    protected $fillable = ['date','ku_ktm','nama','new_ku_ktm','new_satker','nrp','old_satker','skep','status'];
}
