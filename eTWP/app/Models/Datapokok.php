<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datapokok extends Model
{
    use HasFactory;

    protected $table ='datapokok';
    protected $fillable = ['alamat', 'approved_by', 'email', 'is_completed', 'is_retired', 'kd_ktm', 'korp', 'kpr1', 'ku_ktm',
     'militer_status', 'nama', 'nama_rek', 'nm_bank', 'nm_kel1', 'nm_kel2', 'nm_kel3', 'nopok', 'norek', 'npwp', 'nrp', 'nrp_old', 'pangkat', 'phone', 'reject_message',
      'satker','stat_aktif','status','sub_status','tg_lahir','tmt_1','tmt_2','tmt_3','tmt_4','tmt_5','tmt_abri','tmt_henti','tmt_mpp','ur_jab_skep','vcard','status_sub','corps','skep_status','tmt_pkt'];
}
