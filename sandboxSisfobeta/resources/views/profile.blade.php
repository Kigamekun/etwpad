@extends('layouts.base')


@section('content')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-list"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Sandbox KPR</h3>
        <p>API Ebaltab adalah api untuk mengakses informasi riwayat hidup prajurit TNI</p>
    </div>
<br>    
    @if(session('message'))
    <div class="alert alert-success" role="alert">
        {{ session('message') }}
</div>
    
    @endif
    
    <br>
    <div class="page-content">
        <div class="d-flex">
            <div style="flex:1;">
                <form action="{{ route('updateData') }}" method="post">
@csrf
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nama</label>
                        <input type="text" name="nama" class="form-control" value="{{$data->nama}}">
                      </div>
                      
                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">TMT 1</label>
                        <input type="date" name="tmt_1" class="form-control" value="{{$data->tmt_1}}" required>
                      </div>
                      
                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">TMT 2</label>
                        <input type="date" name="tmt_2" class="form-control" value="{{$data->tmt_2}}">

                      </div>

                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">TMT 3</label>
                        <input type="date" name="tmt_3" class="form-control" value="{{$data->tmt_3}}" required>
                      </div>

                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">TMT 4</label>
                        <input type="date" name="tmt_4" class="form-control" value="{{$data->tmt_4}}">
                      </div>
                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">TMT 5</label>
                        <input type="date" name="tmt_5" class="form-control" value="{{$data->tmt_5}}" required>
                      </div>

                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">TMT Pensiun</label>
                        <input type="date" name="tmt_henti" class="form-control" value="{{$data->tmt_henti}}" required>
                      </div>    
                      
                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
                        <input type="date" name="tg_lahir" class="form-control" value="{{$data->tg_lahir}}" required>
                      </div>
                      
                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">No Rekening</label>
                        <input type="text" name="norek" class="form-control" value="{{$data->norek}}" required>
                      </div>
                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nama Rekening</label>
                        <input type="text" name="narek" class="form-control" value="{{$data->narek}}" required>
                      </div>
                      <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nama Bank</label>
                        <input type="text" name="nabank" class="form-control" value="{{$data->nabank}}" required>
                      </div>
                      <br>
                      <button type="submit" class="btn btn-outline-info w-100">Update !</button>
                </form>
            </div>
            <div style="flex:1;">
                 
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    
    <footer>
        <div class="footer clearfix mb-0 text-muted">
            <div class="d-flex justify-content-center">
                <p>2021 &copy; ETWPAD</p>
            </div>
        </div>
    </footer>
</div>

@endsection

@section('js')
    <script>
        $( ".pengajuan" ).click(function() {
                $('#harga').val($(this).attr('data-harga'));
                $('#rumah_id').val($(this).attr('data-rumah'));
});
    </script>
@endsection