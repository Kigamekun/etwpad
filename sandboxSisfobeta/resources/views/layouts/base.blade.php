<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sandbox | Sisfobeta</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{url('assets/vendors/iconly/bold.css') }}">
    <link rel="stylesheet" href="{{url('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{url('assets/vendors/bootstrap-icons/bootstrap-icons.css') }}">
    <link rel="stylesheet" href="{{url('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{url('assets/css/base-custom.css') }}">
    <!--Base Custom Etwpad Style-->
    <link rel="shortcut icon" href="{{url('assets/images/favicon.svg') }}" type="image/x-icon">
@yield('css')
  </head>

<style>
@import url("https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap");
.wrapperSlick {
  width: 100%;
}

.titleHeaderContent {
  margin-bottom: 1rem;
  font-weight: 600;
}

.btnStyle {
  border: 0;
  padding: .5rem 1.5rem;
  color: #fff;
  font-weight: 600;
  border-radius: 10px;
}

.wrapperSlick .slick-list {
  width: 100%;
}

.wrapperSlick .slick-list .slick-track {
  display: flex;
  align-items: center;
}

.wrapperTable {
  width: 100%;
  box-shadow: 0px 0px 11px rgba(0, 0, 0, 0.1);
  border-radius: 15px;
}

.wrapperTable .dataTables_wrapper .row {
  margin-bottom: 1.5rem;
}

.headTable {
  background: #A2A846;
  color: #fff;
}

.filter {
  background: #486245;
  padding: .5rem .5rem;
  display: flex;
  color: #fff;
  align-items: center;
}

.bodyTable tr {
  border: none;
  border-color: transparent;
}

.bodyTable tr td {
  padding: .8rem .5rem;
}

.bodyTable tr td .action {
  display: flex;
  justify-content: center;
}

.bodyTable tr td .action a img {
  max-width: 20px;
}

.bodyTable tr td .action a:not(:last-child) {
  margin-right: 1rem;
}

.sorting::before {
  color: #000000;
}

.sorting::after {
  color: #000000;
}

.searchAndImportWrapper {
  display: flex;
  align-items: center;
  box-shadow: 0px 0px 11px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
  margin-bottom: 2rem;
}

.searchAndImportWrapper .searchInput {
  border: none;
  width: 100%;
  border-bottom: 1px solid #000000;
  padding: .5rem 0;
  max-width: 250px;
}

.searchAndImportWrapper .searchInput::placeholder {
  color: #8E8E8E;
  font-weight: 600;
  font-family: 'Poppins', sans-serif;
}

.nav-tabs {
  border: none;
  position: relative;
}

.nav-tabs .nav-link {
  border: none;
  position: relative;
  margin: 0 1rem;
  color: #ABABAB;
  font-weight: 700;
}

.nav-tabs .nav-link:hover::before {
  width: 100%;
  transition: .3s ease;
}

.nav-tabs .nav-link::before {
  content: '';
  position: absolute;
  bottom: 0;
  width: 0%;
  height: 5px;
  left: 0;
  bottom: -2px;
  display: block;
  background-color: #3C543C;
  transition: .3s ease;
}

.nav-tabs .nav-link.active {
  color: #3E3E3E;
}

.nav-tabs .nav-link.active::before {
  width: 100%;
  transition: .3s ease;
}

.tabsButton {
  margin-bottom: 2rem;
}

.redButton {
  background-color: #DC3545;
}

.greenButton {
  background-color: #28A745;
}

.nameContent {
  font-weight: 700;
  margin-bottom: 2rem;
}

.wrapperData {
  background: #FFFFFF;
  box-shadow: 0px 0px 11px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
}

footer {
  margin-top: 4rem;
  text-align: center;
  padding: 2rem 0;
  box-shadow: 0px -10px 100px rgba(0, 0, 0, 0.1);
}

footer p {
  font-weight: 400;
}

.actionButton {
  display: flex;
  font-family: 'Poppins', sans-serif;
  margin: 2rem 0;
}

.actionButton .btn {
  color: #fff;
  font-weight: 500;
  font-size: 14px;
}

.actionButton .btn:not(:last-child) {
  margin-right: 1rem;
}

.actionButton .btn img {
  max-width: 20px;
  margin-right: .5rem;
  max-height: 20px;
}

.actionButton .editBtn {
  background: #FF832A;
}

.actionButton .saveBtn {
  background: #28A745;
}

.actionButton .deleteBtn {
  background: #DC3545;
}

.view {
  border: none;
  background: #7334FF;
  padding: .3rem .5rem;
  border-radius: 5px;
}

.view img {
  max-width: 25px;
  margin-right: .5rem;
}

.view p {
  color: #fff;
  font-weight: 500;
  font-size: 14px;
}

.wrapperMainData .wrapperData .nav-tabs {
  border-bottom: 1px solid #BCBCBC;
  background: #ECF2F3;
  margin-bottom: 2rem;
}

.wrapperMainData .wrapperData .nav-tabs .nav-link {
  margin: 0;
  margin-bottom: -2px;
}

.wrapperMainData .wrapperData .nav-tabs .nav-link::before {
  display: none;
}

.wrapperMainData .wrapperData .nav-tabs .nav-link.active {
  border: 1px solid transparent;
  border-top-left-radius: .3rem;
  border-top-right-radius: .3rem;
  border-color: #dee2e6 #dee2e6 #fff;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper {
  align-items: flex-start;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .imgProfil {
  display: flex;
  justify-content: center;
  align-items: center;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .imgProfil img {
  width: 100%;
  border-radius: 50%;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData {
  justify-content: space-between;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .table {
  margin-bottom: 0;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .notice {
  background: #A2A846;
  margin-top: 2rem;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .notice p {
  font-family: 'Poppins', sans-serif;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table {
  --bs-table-striped-bg: #ECF2F3;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table thead tr td {
  font-size: 28px;
  font-weight: 600;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table tbody tr {
  display: flex;
  justify-content: space-between;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table tbody tr td {
  width: 100%;
  font-family: 'Poppins', sans-serif;
  font-weight: 400;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table tbody tr td .bunga {
  margin-left: 2rem;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .table > :not(:last-child) > :last-child > * {
  border-bottom: none;
}

.wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .fullWidth {
  width: 100%;
}

@media screen and (max-width: 768px) {
  .mainContent .container-fluid .wrapperContent .nav-tabs {
    align-items: center;
    justify-content: center;
  }
  .mainContent .container-fluid .wrapperContent .nav-tabs .nav-link {
    margin: .5rem 1rem;
  }
}

@media screen and (min-width: 0px) {
  .mainContent {
    padding: 2rem 1rem;
  }
}

@media screen and (min-width: 320px) {
  .mainContent {
    transition: 1s;
  }
  .wrapperTable {
    padding: 1rem;
  }
  .searchAndImportWrapper {
    padding: 2rem;
  }
  .wrapperData {
    padding: 2rem 1rem;
  }
  .wrapperData .nav-tabs .nav-link {
    padding: .5rem 1rem;
    font-size: 12px;
  }
  .tab-content .tab-pane .dataWrapper {
    flex-direction: column;
  }
  .tab-content .tab-pane .dataWrapper .imgProfil {
    width: 100%;
    margin-bottom: 1.5rem;
  }
  .tab-content .tab-pane .dataWrapper .imgProfil img {
    max-width: 120px;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData {
    width: 100%;
    flex-direction: column;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .notice {
    padding: 1.5rem 1rem;
    margin-top: 2rem;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .notice p {
    font-size: 12px;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table {
    width: 100%;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table tbody tr td {
    font-size: 12px;
  }
}

@media screen and (min-width: 768px) {
  .mainContent {
    padding: 6rem 1rem;
  }
  .wrapperTable {
    padding: 2rem;
  }
  .searchAndImportWrapper {
    padding: 3rem;
  }
}

@media screen and (min-width: 992px) {
  .mainContent {
    padding: 6rem 3rem;
  }
  .wrapperTable {
    padding: 3rem;
  }
  .wrapperData {
    padding: 2rem 2rem;
  }
  .wrapperData .nav-tabs .nav-link {
    padding: .5rem 1rem;
    font-size: 14px;
  }
  .tab-content .tab-pane .dataWrapper {
    flex-direction: row;
  }
  .tab-content .tab-pane .dataWrapper .imgProfil {
    width: 30%;
    margin-bottom: 0;
    margin-right: 1rem;
  }
  .tab-content .tab-pane .dataWrapper .imgProfil img {
    max-width: 200px;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData {
    width: 80%;
    flex-direction: column;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table {
    width: 100%;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table tbody tr td {
    font-size: 12px;
  }
}

@media screen and (min-width: 1200px) {
  .tab-content .tab-pane .dataWrapper .imgProfil img {
    max-width: 200px;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData {
    flex-direction: row;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .notice {
    padding: 1.5rem 1rem;
    margin-top: 2rem;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData .notice p {
    font-size: 14px;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table {
    width: 100%;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table:not(:last-child) {
    border-right: 1px solid #DCDCDC;
  }
  .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table tbody tr td {
    font-size: 16px;
    padding: .8rem;
  }
}

@media screen and (max-width: 568px) {
  .wrapperMainData .wrapperData .tab-content .tab-pane .dataWrapper .wrapperTableInfoData table thead tr td {
    font-size: 14px;
  }
}

</style>

<body>
    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between wrapperLogo">
                        <div class="logo">
                            <a href="#"><img src="{{url('assets/img/loading-etwpad.png') }}" alt="Logo" srcset=""></a>
                            <h1 class="nameBrand">ETWPAD SANDBOX</h1>
                        </div>
                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>

                        <li id="dashboard" class="sidebar-item ">
                            <a href="{{ route('home') }}"  class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Home</span>
                            </a>
                        </li>

                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-stack"></i>
                                <span>Akun</span>
                            </a>
                            <ul class="submenu ">

                                <li class="submenu-item ">
                                    <a href="{{ route('account') }}">Akun Sandbox</a>
                                </li>
                                <li class="submenu-item">
                                    <a href="#">Function Token Api</a>
                                </li>

                            </ul>
                        </li>
                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-minecart-loaded"></i>
                                <span>Sandbox</span>
                            </a>
                            <ul class="submenu ">
                                <li id="sandboxbaltab" class="submenu-item ">
                                    <a href="{{ route('baltab.index') }}">E-baltab</a>
                                </li>
                                <li id="sandboxkpr" class="submenu-item ">
                                    <a href="{{ route('kpr.index') }}">E-Kpr</a>
                                </li>
                                <li class="submenu-item ">
                                    <a href="{{ route('twp.index') }}">E-Twp</a>
                                </li>
                            </ul>
                        </li>
                        <li id="api" class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-wrench"></i>
                                <span>Api</span>
                            </a>
                            <ul id="apisub" class="submenu ">
                              <li id="apisitp" class="submenu-item ">
                                <a href="{{ route('api.sitp') }}">SITP</a>
                            </li>
                            <li id="apitwp" class="submenu-item ">
                              <a href="{{ route('api.etwp') }}">E-Iuran</a>
                          </li>
                                <li id="apibaltab" class="submenu-item ">
                                    <a href="{{ route('api.baltab') }}">E-Baltab</a>
                                </li>
                                <li id="apikpr" class="submenu-item ">
                                    <a href="{{ route('api.kpr') }}">E-Kpr</a>
                                </li>
                               
                              
                            </ul>
                        </li>

                        <li id="dashboard" class="sidebar-item ">
                            <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();" class="sidebar-link">
                                <i class="bi bi-grid-fill"></i>
                                <span>Logout</span>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </a>
                        </li>

                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        @yield('content')
    </div>
    <script src="{{url('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{url('assets/js/bootstrap.bundle.min.js') }}"></script>

    <script src="{{url('assets/vendors/apexcharts/apexcharts.js') }}"></script>
    <script src="{{url('assets/js/pages/dashboard.js') }}"></script>

    <script src="{{url('assets/js/main.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

<script>
    var url = window.location.pathname;
    console.log(url);
    if (url == '/sandbox/') {
        $('#dashboard').addClass("active");
    } else if (url == '/sandbox/api/baltab') {
        $('#sandboxkpr').addClass("active");
        $('#api').addClass("active");
         $('#apisub').addClass("active");
    } else if (url == '/sandbox/api/kpr') {
        $('#sandboxbaltab').addClass("active");
        $('#api').addClass("active");
         $('#apisub').addClass("active");
    } else if (url == '/sandbox/api/etwp') {
        $('#sandboxtwp').addClass("active");
        $('#api').addClass("active");
         $('#apisub').addClass("active");
    } else if (url == '/kpr/simulasi') {
        $('#simulasi').addClass("active");
    }
</script>
@yield('js')
</body>

</html>
