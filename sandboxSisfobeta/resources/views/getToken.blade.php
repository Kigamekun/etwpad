@extends('layouts.base')

@section('css')
<link rel="stylesheet" href="{{ url('assets/css/personalAccessToken.css') }}">
@endsection

@section('content')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-list"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Function Token Api</h3>
    </div>
<form action="{{ route('generateToken') }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-primary">Generete New Token</button>
               
                </form>

    @if (!is_null($token = DB::table('token')->where('user_id',Auth::id())->first()))
        
    <div class="wrapperPageContent">
        <div class="head">
            <h5 class="title">Personal Accses Token</h5>
            <div class="wrapperButton">
                

            </div>
        </div>
        <div class="body">
          
            <div class="wrapperApi">
                <div class="apiCode">
                    <i class="bi bi-check"></i>
                    <code id="token">{{$token->token}}</code>
                    <clipboard-copy onclick="copyText(this);" for="new-oauth-token" aria-label="Copy token" tabindex="0" role="button" data-view-component="true">
                        <ion-icon name="copy-outline"></ion-icon>
                    </clipboard-copy>
                </div> 
                <button class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
    @else
    <p class="note"><strong><em>Alert:</em></strong> Anda tidak memiliki Access API token </p>

    @endif
    

    <footer>
        <div class="footer clearfix mb-0 text-muted">
            <div class="d-flex justify-content-center">
                <p>2021 &copy; ETWPAD</p>
            </div>
        </div>
    </footer>
</div>


<script>
  function copyText(element) {
  var range, selection, worked;

  if (document.body.createTextRange) {
    range = document.body.createTextRange();
    range.moveToElementText(element);
    range.select();
  } else if (window.getSelection) {
    selection = window.getSelection();        
    range = document.createRange();
    range.selectNodeContents(element);
    selection.removeAllRanges();
    selection.addRange(range);
  }
  
  try {
    document.execCommand('copy');
    $('#token').css('color','#0D6EFD');
  }
  catch (err) {
    alert('unable to copy text');
  }
}
</script>
@endsection