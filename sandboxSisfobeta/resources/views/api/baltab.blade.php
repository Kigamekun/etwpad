@extends('layouts.base')


@section('content')
    <div id="main">
        <header class="mb-3">
            <a href="#" class="burger-btn d-block d-xl-none">
                <i class="bi bi-list"></i>
            </a>
        </header>

        <div class="page-heading">
            <h1>E-BALTAB</h1>
            <p>API E-BALTAB </p>
        </div>

        <div class="wrapperPageContent">
            <div class="page-content">
                <h2 class="SubContentName" id="dokumentasi">
                    <a href="#dokumentasi">Dokumentasi</a>
                </h2>
                <div class="contentPart">
                    <h5>Versi</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Versi</th>
                                <th>Tanggal</th>
                                <th>Perubahan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">versi 1</code></td>
                                <td>04/10/2021</td>
                                <td>Initial deployment</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <br>

                <h4 class="SubContentName" id="datapembayaran">
                    <a href="#datapembayaran">Data Pembayaran</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/sandbox/api/baltab/datapokokPembayaran</code></td>
                                <td>Return data sandbox semua pengajuan baltab prajurit yang akan dibayarkan</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/baltab/api/pembayaran</code></td>
                                <td>Return data sandbox semua pengajuan baltab prajurit yang akan dibayarkan</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h5 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h5>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <pre class=" language-bash" tabindex="0">
                        <code class=" language-bash">
        <span class="token operator">"0": {</span>

            <span class="token function">
                "id": 1,
                "datapokok_id": "1",
                "status": 3,
                "sprin_date": "221",
                "jumlah": "15000000",
                "bulan": "15",
                "bunga": "1000000",
                "nama_bank": "BTN",
                "no_rekening": "12345678",
                "atas_nama": "Rimuru",
                "tanggal": null,
                "updated_at": null,
                "created_at": null
                        </span>
        <span class="token operator"> },</span>
                        </code>
                    </pre>


                <br>


                <h4 class="SubContentName" id="datatabungan">
                    <a href="#datatabungan">Get Tabungan Prajurit</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/sandbox/api/baltab/getTabungan/{nrp}</code></td>
                                <td>Return data sandbox semua KPR prajurit</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/baltab/api/getData/{nrp}</code></td>
                                <td>Return data sandbox riwayat hidup prajurit baltab</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h5 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h5>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>

                            <tr>
                                <td><code class="highlighter-rouge">Nrp</code></td>
                                <td>NRP untuk akses data prajurit</td>
                                <td>string</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <pre class=" language-bash" tabindex="0">
                        <code class=" language-bash">
        <span class="token operator">"0": {</span>

            <span class="token function">
                "message": "Success",
                "statusCode": 200,
                "prajurit": {
                    "id": 1,
                    "user_id": 1,
                    "nrp": "123",
                    "nama": "Rimuru",
                    "tmt_1": "1989-07-02",
                    "tmt_2": "2019-05-24",
                    "tmt_3": "1972-11-06",
                    "tmt_4": "1988-03-05",
                    "tmt_5": "2020-05-12",
                    "tmt_henti": "1991-12-18",
                    "tg_lahir": "1994-03-13",
                    "created_at": null,
                    "updated_at": "2021-10-23T02:35:58.000000Z",
                    "is_pengajuan": 0,
                    "is_complate": 0,
                    "tgl_pensiun": null,
                    "narek": "Excepturi nulla comm",
                    "norek": "1234567890",
                    "nabank": "Occaecat dolor esse",
                    "total": null,
                    "bunga": null,
                    "status": 0
                },
                "tabungan": {
                    "bulan": 30,
                    "jumlah": "84808.77",
                    "bunga": 9808.77
                }
                        </span>
        <span class="token operator"> },</span>
                        </code>
                    </pre>

                <h4 class="SubContentName" id="ubahstatus">
                    <a href="#ubahstatus">Ubah Status Pembayaran</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Method</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/kpr/sanbox/baltab/cairkan</code></td>
                                <td>POST</td>
                                <td>Mengubah status pembayaran dari 3 menjadi 4 sesuai dengan bank yang mencairkan</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/baltab/api/cairkan</code></td>
                                <td>POST</td>
                                <td>Mengubah status pembayaran dari 3 menjadi 4 sesuai dengan bank yang mencairkan</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h5 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h5>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td><code class="highlighter-rouge">datapokok_ids</code></td>
                                <td>berupa string array seperti { "datapokok_ids" : "1,2,3,4,5" }</td>
                                <td>string</td>
                            </tr>

                        </tbody>
                    </table>
                </div>







                <br>


                <h4 class="SubContentName" id="datacair">
                    <a href="#datacair">Get Datapokok Cair</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/sandbox/api/baltab/datapokokCair</code></td>
                                <td>Return data sandbox pencairan dari semua KPR prajurit</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/baltab/api/pencairan</code></td>
                                <td>Return semua data baltab yang telah dicairkan</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h5 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h5>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>

                            <tr>
                                <td><code class="highlighter-rouge">Nrp</code></td>
                                <td>NRP untuk akses data prajurit</td>
                                <td>string</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <pre class=" language-bash" tabindex="0">
                        <code class=" language-bash">
        <span class="token operator">"0": {</span>

            <span class="token function">
                "id": 2,
                "datapokok_id": "2",
                "status": 4,
                "sprin_date": "221",
                "jumlah": "15000000",
                "bulan": "15",
                "bunga": "1000000",
                "nama_bank": "BTN",
                "no_rekening": "12345678",
                "atas_nama": "Rimuru",
                "tanggal": null,
                "updated_at": null,
                "created_at": null  
                        </span>
        <span class="token operator"> },</span>
                        </code>
                    </pre>

                <br>
                <br>
                <br>


                <h4 class="SubContentName" id="verifiednorek">
                    <a href="#verifiednorek">Verified No Rekening</a>
                </h4>

                <p class="note"><strong><em>Note:</em></strong> Hit API dari bank yang digunakan untuk mengecek no
                    rekening para prajurit</p>

                <div class="contentPart">



                    <div class="contentPart contentTabs">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                                aria-selected="true">BTN</button>

                            <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
                                data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile"
                                aria-selected="false">BRI</button>

                            <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
                                data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact"
                                aria-selected="false">BSI</button>
                        </div>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                aria-labelledby="nav-home-tab">

                                {{-- <table class="table2">
                            <thead>
                                <tr>
                                    <th>Visa</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                            </tbody>
                    </table> --}}

                                <div class="contentPart">
                                    <h5>Endpoints</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Type</th>
                                                <th>Endpoint</th>
                                                <th>Kegunaan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Dev</td>
                                                <td><code class="highlighter-rouge">BTN ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BTN</td>
                                            </tr>

                                            <tr>
                                                <td>Prod</td>
                                                <td><code class="highlighter-rouge">BTN ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BTN</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="contentPart">
                                    <h5>Required Parameter</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Deskripsi</th>
                                                <th>Tipe Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><code class="highlighter-rouge">nrp</code></td>
                                                <td>nrp prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>


                                            <tr>
                                                <td><code class="highlighter-rouge">no rekening</code></td>
                                                <td>no rekening prajurit untuk menambahkan data secara realtime pada
                                                    prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>

                                            <tr>
                                                <td><code class="highlighter-rouge">nama rekening</code></td>
                                                <td>nama rekening yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>

                                         

                                            <tr>
                                                <td><code class="highlighter-rouge">nama prajurit</code></td>
                                                <td>nama prajurit yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                <div class="contentPart">
                                    <h5>Endpoints</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Type</th>
                                                <th>Endpoint</th>
                                                <th>Kegunaan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Dev</td>
                                                <td><code class="highlighter-rouge">BRI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BRI</td>
                                            </tr>

                                            <tr>
                                                <td>Prod</td>
                                                <td><code class="highlighter-rouge">BRI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BRI</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="contentPart">
                                    <h5>Required Parameter</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Deskripsi</th>
                                                <th>Tipe Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><code class="highlighter-rouge">nrp</code></td>
                                                <td>nrp prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>


                                            <tr>
                                                <td><code class="highlighter-rouge">no rekening</code></td>
                                                <td>no rekening prajurit untuk menambahkan data secara realtime pada
                                                    prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>

                                            <tr>
                                                <td><code class="highlighter-rouge">nama rekening</code></td>
                                                <td>nama rekening yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>

                                            
                                            <tr>
                                                <td><code class="highlighter-rouge">nama prajurit</code></td>
                                                <td>nama prajurit yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                                <div class="contentPart">
                                    <h5>Endpoints</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Type</th>
                                                <th>Endpoint</th>
                                                <th>Kegunaan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Dev</td>
                                                <td><code class="highlighter-rouge">BSI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BSI</td>
                                            </tr>

                                            <tr>
                                                <td>Prod</td>
                                                <td><code class="highlighter-rouge">BSI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BSI</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="contentPart">
                                    <h5>Required Parameter</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Deskripsi</th>
                                                <th>Tipe Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><code class="highlighter-rouge">nrp</code></td>
                                                <td>nrp prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>


                                            <tr>
                                                <td><code class="highlighter-rouge">no rekening</code></td>
                                                <td>no rekening prajurit untuk menambahkan data secara realtime pada
                                                    prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>

                                            <tr>
                                                <td><code class="highlighter-rouge">nama rekening</code></td>
                                                <td>nama rekening yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>

                                            <tr>
                                                <td><code class="highlighter-rouge">nama prajurit</code></td>
                                                <td>nama prajurit yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                {{-- <table class="table2">
                <thead>
                    <tr>
                        <th>Input</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Expiry Month</td>
                        <td><code>01</code> (or any month)</td>
                    </tr>
                    <tr>
                        <td>Expiry Year</td>
                        <td><code>2025</code> (or any future year)</td>
                    </tr>
                    <tr>
                        <td>CVV</td>
                        <td><code>123</code></td>
                    </tr>
                    <tr>
                        <td>OTP/3DS</td>
                        <td><code>112233</code></td>
                    </tr>
                    <tr>
                        <td>Card Number</td>
                        <td>Refer to table given below.</td>
                    </tr>
                </tbody>
            </table> --}}

            </div>
            <div class="sidebarRight">
                <div class="wrapper-sidebar-right">
                    <h5 class="sidebar-right-title">In This Pages</h5>
                    <ul class="sidebar-right-underlist">
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#dokumentasi">Dokumentasi</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#datapembayaran">Data pembayaran</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#datatabungan">Data tabungan</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#ubahstatus">Ubah status pembayaran</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#datacair">Data Cair</a>
                        </li>

                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#verifiednorek">Verified No rekening</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <footer>
            <div class="footer clearfix mb-0 text-muted">
                <div class="d-flex justify-content-center">
                    <p>2021 &copy; ETWPAD</p>
                </div>
            </div>
        </footer>
    </div>
@endsection
