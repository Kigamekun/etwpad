@extends('layouts.base')


@section('content')

<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-list"></i>
        </a>
    </header>

    <div class="page-heading">
        <h1>E-TWP</h1>
        <p>API ETWP adalah api untuk mengakses informasi riwayat hidup prajurit TNI</p>
    </div>

    <div class="wrapperPageContent">
        <div class="page-content">
            <h2 class="SubContentName" id="dokumentasi">
                <a href="#dokumentasi">Dokumentasi</a>
            </h2>
            <div class="contentPart">
                <h5>Versi</h5>
                <table>
                    <thead class="headWhite">
                        <tr>
                            <th>Versi</th>
                            <th>Tanggal</th>
                            <th>Perubahan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><code class="highlighter-rouge">versi 1</code></td>
                            <td>04/10/2021</td>
                            <td>Initial deployment</td>
                        </tr>
                    </tbody>
                </table>
            </div>





            <h4 class="SubContentName" id="datacair">
                <a href="#datacair">Passing Data Gaji</a>
            </h4>

            <p></p>

            <div class="contentPart">
                <h5>Endpoints</h5>
                <table>
                    <thead class="headWhite">
                        <tr>
                            <th>Type</th>
                            <th>Endpoint</th>
                            <th>Kegunaan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Dev</td>
                            <td><code class="highlighter-rouge">/sandbox/twp/getDataGaji/{nrp}</code></td>
                            <td>Return data gaji bulanan dari setiap prajurit</td>
                        </tr>

                        <tr>
                            <td>Prod</td>
                            <td><code class="highlighter-rouge">/eTWP/getDataGaji/{nrp}</code></td>
                            <td>Return data gaji bulanan dari setiap prajurit</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="SubContentName" id="reqParameter">
                <a href="#reqParameter">Required Parameter</a>
            </h5>
            <div class="contentPart">
                <h5>Example API Fields</h5>
                <table>
                    <thead class="headWhite">
                        <tr>
                            <th>Parameter</th>
                            <th>Deskripsi</th>
                            <th>Tipe Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><code class="highlighter-rouge">TOKEN</code></td>
                            <td>Token untuk access API</td>
                            <td>string</td>
                        </tr>

                        <tr>
                            <td><code class="highlighter-rouge">Nrp</code></td>
                            <td>NRP untuk akses data prajurit</td>
                            <td>string</td>
                        </tr>

                    </tbody>
                </table>
            </div>
           

            <pre class=" language-bash" tabindex="0">
                <code class=" language-bash">
<span class="token operator">"0": {</span>
<span class="token function"> 
    "id": 2,
    "nrp": "1030037191082",
    "tanggalgaji": "2016-01-01",
    "rekening": "001801001870302",
    "status" : "0001",
    "iuran" : "50000",
    "nama_rekening" : "Burhan",
    </span>
    <span class="token operator"> },</span>
                </code>
            </pre>
            <br>
            <br>
            <br>



        </div>
        <div class="sidebarRight">
            <div class="wrapper-sidebar-right">
                <h5 class="sidebar-right-title">In This Pages</h5>
                <ul class="sidebar-right-underlist">
                    <li class="sidebar-right-list">
                        <a class="sidebar-right-link" href="#dokumentasi">Dokumentasi</a>
                    </li>
                    <li class="sidebar-right-list">
                        <a  class="sidebar-right-link" href="#reqParameter">Required Parameter</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>



    <footer>
        <div class="footer clearfix mb-0 text-muted">
            <div class="d-flex justify-content-center">
                <p>2021 &copy; ETWPAD</p>
            </div>
        </div>
    </footer>
</div>
@endsection
