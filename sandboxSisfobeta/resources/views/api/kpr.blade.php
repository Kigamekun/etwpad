@extends('layouts.base')


@section('content')
    <div id="main">
        <header class="mb-3">
            <a href="#" class="burger-btn d-block d-xl-none">
                <i class="bi bi-list"></i>
            </a>
        </header>

        <div class="page-heading">
            <h1>E-KPR</h1>
            <p>API E-KPR </p>
        </div>

        <div class="wrapperPageContent">
            <div class="page-content">
                <h2 class="SubContentName" id="dokumentasi">
                    <a href="#dokumentasi">Dokumentasi</a>
                </h2>
                <div class="contentPart">
                    <h5>Versi</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Versi</th>
                                <th>Tanggal</th>
                                <th>Perubahan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">versi 1</code></td>
                                <td>04/10/2021</td>
                                <td>Initial deployment</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <br>
               
                <h4 class="SubContentName" id="datakpr">
                    <a href="#datakpr">Data KPR</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/sandbox/api/kpr/dataKpr</code></td>
                                <td>Return data sandbox semua KPR prajurit</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/kpr/api/dataKpr/{status}</code></td>
                                <td>Return data KPR prajurit berdasarkan statusnya</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h5 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h5>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>

                            <tr>
                                <td><code class="highlighter-rouge">Status (PRODUCTION)</code></td>
                                <td>Status untuk profiling data KPR {0 : Manual, 1 : Massdebet BRI, 2 : Belum DiApprove, 3 : Massdebet BTN, 4 : Lunas, 5 : Meninggal, all : return All }</td>
                                <td>string</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <pre class=" language-bash" tabindex="0">
                    <code class=" language-bash">
    <span class="token operator">"0": {</span>

        <span class="token function">
        "id": 1,
        "nrp": "12345",
        "rumah_id": 34,
        "nama": "Do aliquip commodi n",
        "user_id": 1,
        "alamat": null,
        "tahap": 0,
        "pinjaman": 1600000000,
        "jk_waktu": 156,
        "tmt_angsuran": "2021-10-21",
        "jml_angs": 15062000,
        "angs_ke": 1,
        "angsuran_masuk": null,
        "angs_masuk_btn": null,
        "angs_masuk_manual": null,
        "tunggakan": 1,
        "jml_tunggakan": 15062000,
        "tunggakan_pokok": null,
        "tunggakan_bunga": null,
        "keterangan": null,
        "rekening": "1234567890",
        "rekening_kredit": null,
        "nama_bank": "BRI",
        "rek_bri": null,
        "rek_btn": null,
        "status": null,
        "bunga": 1248000000,
        "pokok": 1101672000,
        "sisa_pinjaman_pokok": null,
        "piutang_bunga": null,
        "piutang_pokok": null,
        "created_at": "2021-10-21T17:49:21.000000Z",
        "updated_at": "2021-10-21T17:49:21.000000Z"
                    </span>
    <span class="token operator"> },</span>
                    </code>
                </pre>


                <br>
               

                <h4 class="SubContentName" id="datakprspec">
                    <a href="#datakprspec">Data KPR Spesifik</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/kpr/sanbox/dataKpr/{nrp}</code></td>
                                <td>Return data sandbox KPR Spesifik prajurit</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/kpr/api/detailKpr/{nrp}</code></td>
                                <td>Return data KPR spesifik prajurit</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h5 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h5>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td><code class="highlighter-rouge">nrp</code></td>
                                <td>nrp untuk spesifik user</td>
                                <td>integer</td>
                            </tr>

                        </tbody>
                    </table>
                </div>



                <br>
               


                <h4 class="SubContentName" id="datatransaksi">
                    <a href="#datatransaksi">Data Transaksi</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/kpr/sanbox/dataTransaksi</code></td>
                                <td>Return data sandbox transaksi pembayaran KPR semua prajurit</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/kpr/api/dataTransaksi</code></td>
                                <td>Return data transaksi pembayaran KPR dari semua prajurit</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h2 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h2>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                {{-- <pre class=" language-bash" tabindex="0">
                    <code class=" language-bash">
                        <span class="token operator">"0": {</span>

                     <span class="token function"> "atas_nama": "AGS",
                        "jumlah": "3213213",
                        "total_bulan": "12",
                        "nama_bank": "BRI",
                        "no_rekening": "4324244",
                        "data_prajurit": {
                            "nama_prajurit": "Agus",
                            "nrp": "333",
                            "tanggal pensiun": "2034-08-13"
                        }</span>
                      <span class="token operator"> },</span>
                    </code>
                </pre> --}}

                

                <br>
               


                <h4 class="SubContentName" id="datatransaksispec">
                    <a href="#datatransaksispec">Data Spesifik Transaksi</a>
                </h4>

                <div class="contentPart">
                    <h5>Endpoints</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Type</th>
                                <th>Endpoint</th>
                                <th>Kegunaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dev</td>
                                <td><code class="highlighter-rouge">/api/kpr/dataTransaksi/{nrp}</code></td>
                                <td>Return data sandbox transaksi KPR salah satu prajurit</td>
                            </tr>

                            <tr>
                                <td>Prod</td>
                                <td><code class="highlighter-rouge">/kpr/api/dataTransaksi/{nrp}</code></td>
                                <td>return data Transaksi spesifik dari prajurit pemilik KPR</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h2 class="SubContentName" id="reqParameter">
                    <a href="#reqParameter">Required Parameter</a>
                </h2>
                <div class="contentPart">
                    <h5>Example API Fields</h5>
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                                <th>Tipe Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code class="highlighter-rouge">TOKEN</code></td>
                                <td>Token untuk access API</td>
                                <td>string</td>
                            </tr>

                            <tr>
                                <td><code class="highlighter-rouge">nrp</code></td>
                                <td>Parameter untuk spesifik user</td>
                                <td>string</td>
                            </tr>


                        </tbody>
                    </table>
                </div>
                <pre class=" language-bash" tabindex="0">
                    <code class=" language-bash">
<span class="token operator">"0": {</span>

                     <span class="token function"> 
        "id": 2,
        "nrp": "1030037191082",
        "kodecabang": "0018",
        "tanggalproses": "2016-01-04",
        "rekeningkredit": "001801001870302",
        "batch": "2016010483220",
        "remarks": "KPR TNI",
        "matauang": "IDR",
        "tanggal": "2016-01-04 01:00:00",
        "batch_1": "2016010483220",
        "iddetail": "1",
        "rekeningdebet": "006201041812501",
        "amountdebet": "1293000",
        "amountcharge": 0,
        "remarkstransaksi": "TWP:1030037191082:0116",
        "status": "0001",
        "deskripsi": "Transaction Successful",
        "agemsg": "",
        "journalseq": "",
        "nama": "RIZKY ADITYA",
        "namabrinets": "RIZKY ADITYA"
        </span>
        <span class="token operator"> },</span>
                    </code>
                </pre>

                <br>
                <br>
                <br>



                <h4 class="SubContentName" id="scdata">
                    <a href="#scdata">Scratch data transaksi</a>
                </h4>

                <p class="note"><strong><em>Note:</em></strong> Hit API dari bank yang terkait untuk mendapatkan
                    data transaksi terbaru yang nantinya akan digunakan untuk mengupdate pembayaran KPR</p>

                <div class="contentPart">



                    <div class="contentPart contentTabs">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                                aria-selected="true">BTN</button>

                            <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
                                data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile"
                                aria-selected="false">BRI</button>

                            <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
                                data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact"
                                aria-selected="false">BSI</button>
                        </div>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                aria-labelledby="nav-home-tab">

                                {{-- <table class="table2">
                            <thead>
                                <tr>
                                    <th>Visa</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Authentication
                                        <br>
                                        <em>Cardholder is 3DS ready</em>
                                    </td>
                                    <td>
                                        <strong>Accept Transaction:</strong>  4811 1111 1111 1114
                                        <br>
                                        <strong>Denied by Bank Transaction:</strong>  4811 1111 1111 1114
                                    </td>
                                </tr>
                            </tbody>
                    </table> --}}

                                <div class="contentPart">
                                    <h5>Endpoints</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Type</th>
                                                <th>Endpoint</th>
                                                <th>Kegunaan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Dev</td>
                                                <td><code class="highlighter-rouge">BTN ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BTN</td>
                                            </tr>

                                            <tr>
                                                <td>Prod</td>
                                                <td><code class="highlighter-rouge">BTN ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BTN</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="contentPart">
                                    <h5>Required Parameter</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Deskripsi</th>
                                                <th>Tipe Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><code class="highlighter-rouge">nrp</code></td>
                                                <td>nrp prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>


                                            <tr>
                                                <td><code class="highlighter-rouge">no rekening</code></td>
                                                <td>no rekening prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>

                                            <tr>
                                                <td><code class="highlighter-rouge">nama rekening</code></td>
                                                <td>nama rekening yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                <div class="contentPart">
                                    <h5>Endpoints</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Type</th>
                                                <th>Endpoint</th>
                                                <th>Kegunaan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Dev</td>
                                                <td><code class="highlighter-rouge">BRI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BRI</td>
                                            </tr>

                                            <tr>
                                                <td>Prod</td>
                                                <td><code class="highlighter-rouge">BRI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BRI</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="contentPart">
                                    <h5>Required Parameter</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Deskripsi</th>
                                                <th>Tipe Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><code class="highlighter-rouge">nrp</code></td>
                                                <td>nrp prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>


                                            <tr>
                                                <td><code class="highlighter-rouge">no rekening</code></td>
                                                <td>no rekening prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>

                                            <tr>
                                                <td><code class="highlighter-rouge">nama rekening</code></td>
                                                <td>nama rekening yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                                <div class="contentPart">
                                    <h5>Endpoints</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Type</th>
                                                <th>Endpoint</th>
                                                <th>Kegunaan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Dev</td>
                                                <td><code class="highlighter-rouge">BSI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BSI</td>
                                            </tr>

                                            <tr>
                                                <td>Prod</td>
                                                <td><code class="highlighter-rouge">BSI ..</code></td>
                                                <td>Return data Transaksi terbaru prajurit dari bank BSI</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="contentPart">
                                    <h5>Required Parameter</h5>
                                    <table>
                                        <thead class="headWhite">
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Deskripsi</th>
                                                <th>Tipe Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><code class="highlighter-rouge">nrp</code></td>
                                                <td>nrp prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>


                                            <tr>
                                                <td><code class="highlighter-rouge">no rekening</code></td>
                                                <td>no rekening prajurit untuk menambahkan data secara realtime pada prajurit
                                                    tertentu</td>
                                                <td>string</td>
                                            </tr>

                                            <tr>
                                                <td><code class="highlighter-rouge">nama rekening</code></td>
                                                <td>nama rekening yang digunakan untuk pembayaran KPR</td>
                                                <td>string</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                {{-- <table class="table2">
                <thead>
                    <tr>
                        <th>Input</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Expiry Month</td>
                        <td><code>01</code> (or any month)</td>
                    </tr>
                    <tr>
                        <td>Expiry Year</td>
                        <td><code>2025</code> (or any future year)</td>
                    </tr>
                    <tr>
                        <td>CVV</td>
                        <td><code>123</code></td>
                    </tr>
                    <tr>
                        <td>OTP/3DS</td>
                        <td><code>112233</code></td>
                    </tr>
                    <tr>
                        <td>Card Number</td>
                        <td>Refer to table given below.</td>
                    </tr>
                </tbody>
            </table> --}}

            </div>
            <div class="sidebarRight">
                <div class="wrapper-sidebar-right">
                    <h5 class="sidebar-right-title">In This Pages</h5>
                    <ul class="sidebar-right-underlist">
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#dokumentasi">Dokumentasi</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#datakpr">Data KPR</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#datakprspec">Data KPR Spesifik</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#datatransaksi">Data Transaksi</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#datatransaksispec">Data Transaksi Spesifik</a>
                        </li>
                        <li class="sidebar-right-list">
                            <a class="sidebar-right-link" href="#scdata">Scratch Data</a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>


        <footer>
            <div class="footer clearfix mb-0 text-muted">
                <div class="d-flex justify-content-center">
                    <p>2021 &copy; ETWPAD</p>
                </div>
            </div>
        </footer>
    </div>
@endsection
