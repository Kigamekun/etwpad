@extends('layouts.base')


@section('content')

<style>
    #kpr {
        border-collapse: collapse;
    }
    #kpr tr td{
        border: none;
    }
    
    .textInformation {
        display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;  /* Number of lines displayed before it truncate */
     overflow: hidden;

    }
    .nameProperty{
        display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 1;  /* Number of lines displayed before it truncate */
     overflow: hidden;
    }

    .images-properti {
        min-height: 300px;
        max-height: 300px;
    }
</style>
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-list"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Sandbox KPR</h3>
        <p>API Ebaltab adalah api untuk mengakses informasi riwayat hidup prajurit TNI</p>
    </div>
    <p class="note"><strong><em>Note:</em></strong> User yang digunakan harus dalam keadaan lengkap</p>

<br>
    @if(session('message'))
    <div class="alert alert-success" role="alert">
        {{ session('message') }}
</div>

    @endif

    <br>
    <div class="page-content">
        <div class="row row-cols-1 row-cols-md-2 g-4">
                {{-- @foreach ($rumah as $item)
                <div class="col">
                    <div class="card">
                      <img style="height: 421px;" src="{{ $item->GAMBAR }}" class="card-img-top" alt="...">
                      <div class="card-body">
                        <h5 class="card-title">{{ $item->NAMA_PROPER }}</h5>
                        <p  style="font-weight: bold" class="card-text">{{ $item->ALAMAT }}</p>
                        <br>
                        <p class="card-text">{{ $item->DESKRIPSI }}</p>
                        <br>

                          

                      </div>
                    </div>
                  </div>




                @endforeach
 --}}



                  

          </div>
          <div class="contentPart contentTabs">
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <button class="nav-link active" 
                id="nav-kprProperti-tab" 
                data-bs-toggle="tab" 
                data-bs-target="#nav-kprProperti" 
                type="button" 
                role="tab" 
                aria-controls="nav-kprProperti" 
                aria-selected="true">Ajukan KPR</button>

             @if (!is_null(DB::table('kpr')->where('user_id',Auth::id())->first()))
             <button class="nav-link" 
             id="nav-data-tab" 
             data-bs-toggle="tab" 
             data-bs-target="#nav-data" 
             type="button" 
             role="tab" 
             aria-controls="nav-data" 
             aria-selected="false">Data Prajurit</button>
             @endif
            </div>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" 
                id="nav-kprProperti" 
                role="tabpanel" 
                aria-labelledby="nav-kprProperti-tab">
                    <center>
                        <div class="wrapperCard" style="justify-content: center">
                        
                        
                            @foreach ($rumah as $item)
                            <div class="wrapperProperty">
                                <div class="cardProperti">
                                    <img src="{{ $item->GAMBAR }}" alt="" class="images-properti">
                                    <div class="information">
                                        <h5 class="nameProperty">{{ $item->NAMA_PROPER }}</h5>
                                        <p class="lokasiPerumahan">{{ $item->ALAMAT }}</p>
                                        <div class="detailInformation">
                                            <p class="textInformation">{{ $item->DESKRIPSI }}</p>
                                        </div>
                                    </div>
                                    <div class="wrapperButton">
                                        @if (!is_null(DB::table('kpr')->where('user_id',Auth::id())->first()))
    
                                        <center><button class="btn btn-info pengajuan" data-harga="{{$item->HARGA}}" data-rumah="{{$item->id}}" type="button" data-bs-toggle="modal"  data-bs-target="#pengajuan" disabled>Ajukan KPR</button></center>
                                        @else
                                        <center><button class="btn btn-info pengajuan" data-harga="{{$item->HARGA}}" data-rumah="{{$item->id}}" type="button" data-bs-toggle="modal"  data-bs-target="#pengajuan">Ajukan KPR</button></center>
                                        @endif
                                    </div>
                                </div>
                            </div>
    
                            @endforeach
    
                            
                        </div>
                    </center>
                </div>
            
                @isset($kpr)
                <div class="tab-pane fade" 
                id="nav-data" 
                role="tabpanel" 
                aria-labelledby="nav-data-tab">
                <div class="wrapperTable">
                    <table id="kpr" cellspacing="0" rules="none" cellpadding="0" style="border: none;">
                        
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>{{ $kpr->nama }}</td>

                            </tr>
                           


                            <tr>
                                <td>NRP</td>
                                <td>:</td>
                                <td>{{ $kpr->nrp }}</td>

                            </tr>
                          
                            <tr>
                                <td>Tahap</td>
                                <td>:</td>
                                <td>{{ $kpr->tahap }}</td>
                            </tr>

                            <tr>
                                <td>Angsuran Masuk</td>
                                <td>:</td>
                                <td>{{ $kpr->angsuran_masuk }}</td>
                            </tr>



                            <tr>
                                <td>Jumlah Pinjaman</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($kpr->pinjaman, 2, ',', '.') }}</td>
                            </tr>


                            <tr>
                                <td>Jangka waktu</td>
                                <td>:</td>
                                <td>{{ $kpr->jk_waktu }}</td>
                            </tr>
                            <tr>
                                <td>TMT Angsuran</td>
                                <td>:</td>
                                <td>{{ $kpr->tmt_angsuran }}</td>
                            </tr>

                            <tr>
                                <td>Jumlah Angsuran</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($kpr->jml_angs, 2, ',', '.') }}</td>


                            </tr>

                            <tr>
                                <td>Angsuran ke </td>
                                <td>:</td>
                                <td>{{ $kpr->angs_ke }}</td>
                            </tr>

                            <tr>
                                <td>Tunggakan</td>
                                <td>:</td>
                                <td>{{ $kpr->tunggakan }}</td>
                            </tr>


                            <tr>
                                <td>Jumlah Tunggakan</td>
                                <td>:</td>
                                <td>Rp.{{ number_format($kpr->jml_tunggakan, 2, ',', '.') }}</td>

                            </tr>
                            <tr>
                                <td>Keterangan</td>
                                <td>:</td>
                                <td>{{ $kpr->keterangan }}</td>

                            </tr>
                            <tr>
                                <td>Sisa pinjaman pokok</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($kpr->sisa_pinjaman_pokok, 2, ',', '.') }}</td>

                            </tr>

                            <tr>
                                <td>Pokok Bulan ini</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($kpr->pokok_bulan_ini, 2, ',', '.') }}</td>

                            </tr>
                            <tr>
                                <td>Bunga bulan ini</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($kpr->bunga_bulan_ini, 2, ',', '.') }}</td>

                            </tr>
                            <tr>
                                <td>Tunggakan Bunga</td>
                                <td>:</td>
                                <td>Rp.{{ number_format($kpr->tunggakan_bunga, 2, ',', '.') }}</td>

                            </tr>
                            <tr>
                                <td>Outstanding</td>
                                <td>:</td>
                                <td>Rp.{{ number_format($kpr->piutang_pokok + $kpr->tunggakan_bunga, 2, ',', '.') }}
                                </td>

                            </tr>
                       
                    </table>
                </div>

                <div class="wrapperTable">
                    <table>
                        <thead class="headWhite">
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Jumlah</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                          
                            @foreach (DB::table('TrxBri')->where('nrp',$kpr->nrp)->get() as $key => $item)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$item->tanggal}} <span class="time">00:00:00</span></td>
                                <td>Rp {{$kpr->jml_angs}}</td>
                                <td>Transaksi Sukses</td>
                            </tr>
                           
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
                <br>
                <br>
                <center>
                    <button class="btn btn-success" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Bayar</button>
                <a href="{{ route('kpr.deleteKpr') }}" class="btn btn-danger">Hapus KPR</a>
                </center>
            </div>
                @endisset
               
            </div>
            
          
        </div>
    </div>

    <footer>
        <div class="footer clearfix mb-0 text-muted">
            <div class="d-flex justify-content-center">
                <p>2021 &copy; ETWPAD</p>
            </div>
        </div>
    </footer>
</div>

@isset($transaksi)
    
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
                @if (session()->has('error_update'))
                    <span>{{ session('error_update') }}</span>
                @endif
                <form id="form" action="{{ route('kpr.bayarPost') }}" method="POST">
                    @csrf
                    <div class="table-responsive">

                        <table class="table table-bordered">
                            <tr class="text-center">
                                <th rowspan="2">TAHUN</th>
                                <th colspan="12">BULAN ANGSURAN</th>
                            </tr>
                            <tr class="text-center">
                                <th>JAN</th>
                                <th>FEB</th>
                                <th>MAR</th>
                                <th>APR</th>
                                <th>MEI</th>
                                <th>JUN</th>
                                <th>JUL</th>
                                <th>AGS</th>
                                <th>SEPT</th>
                                <th>OKT</th>
                                <th>NOP</th>
                                <th>DES</th>
                            </tr>
                            @for ($i = 0; $i < count($years); $i++)
                                <tr class="text-center">
                                    <td>{{ $years[$i] }}</td>
                                    @for ($j = 1; $j <= 12; $j++)
                                        <td>
                                            @forelse ($transaksi as $z => $t)

                                                @if (date('n', strtotime($t->tanggal)) == $j && date('Y', strtotime($t->tanggal)) == $years[$i])
                                                    <input type="checkbox" class="check"
                                                        @foreach ($transaksi as $t) {{ date('n', strtotime($t->tanggal)) == $i + 1 ? '' : 'checked disabled' }} @endforeach>
                                                @break(true)
                                            @endif

                                            @if ($z == count($transaksi) - 1)
                                                @if ($i == count($years) - 1 && $j < date('n', strtotime($kpr->tmt_angsuran)))

                                                @else
                                                    <input type="checkbox" class="check"
                                                        name="data[{{ $years[$i] }}][]" id="{{ $j }}"
                                                        value="{{ $j }}"
                                                        data-date="{{ $j . '/' . $years[$i] }}">
                                                @break(true)
                                            @endif
                                    @endif
                                    @empty
                                        @if (!($i == (count($years) - 1) && $j < date('n', strtotime($kpr->
                                            tmt_angsuran))))
                                            <input type="checkbox" class="check"
                                                name="data[{{ $years[$i] }}][]" id="{{ $j }}"
                                                value="{{ $j }}" data-date="{{ $j . '/' . $years[$i] }}">
                                @endif
            @endforelse
            </td>
            @endfor
            </tr>
            @endfor
            </table>


        </div>

        </form>

        {{-- <div class="text-right mt-5">
            <button  class=" btn btn-primary">Bayar </button>
        </div> --}}
    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" id="btn-submit" class="btn btn-primary">Bayar</button>
        </div>
      </div>
    </div>
  </div>

  @endisset
  <!-- Modal -->
  <div class="modal fade" id="pengajuan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog " >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Form Pengajuan</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ route('kpr.pinjaman') }}" method="post">
            @csrf
           <input type="hidden" name="rumah_id" id="rumah_id">

        <div class="modal-body">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Harga</label>
                <input type="number" name="harga" id="harga" class="form-control" readonly>
              </div>
              <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Bunga</label>
                <input type="text" name="bunga" id="bunga" class="form-control" value="6%" disabled>
              </div>
              <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Jangka Waktu</label>
                <input type="number" name="jk_waktu" id="jk_waktu" class="form-control" required>
              </div>


        </div>
        <div class="modal-footer">

          <button type="submit" class="btn btn-primary">Ajukan</button>
        </div>
    </form>
      </div>
    </div>
  </div>


@endsection

@section('js')
    <script>
        $( ".pengajuan" ).click(function() {
                $('#harga').val($(this).attr('data-harga'));
                $('#rumah_id').val($(this).attr('data-rumah'));
});
    </script>



<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


@isset($transaksi)
<script>
    var norek = @json($kpr->rekening);
    var besar_angsuran = @json($besar_angsuran);
    $('#btn-submit').on('click', function(e) {

        e.preventDefault();

        var form = $('#form');
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        })

        var arr = [];
        $('input.check:checkbox:checked').each(function() {
            arr.push($(this).attr('data-date'));
        });

        var textnya = "Kamu akan membayar pinjaman :";
        var jml = 0;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                textnya += "<br> " + arr[i];
                jml++;
            }

        }
        textnya += "<br><br> dengan no rekening :" + norek;
        textnya += "<br> Total : " + besar_angsuran * jml;



        swalWithBootstrapButtons.fire({
            title: 'Apakah anda yakin ?',

            icon: 'warning',
            html: textnya,
            showCancelButton: true,
            confirmButtonText: 'Ya lanjutkan!',
            cancelButtonText: 'Tidak , batalkan!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    icon: 'success',
                    title: 'Terkirim...',
                    showConfirmButton: false,
                    timer: 2000,
                    text: 'Pembayaran anda sedang diproses !',

                })

                setTimeout(function() {
                    form.submit();
                }, 3000);
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    showConfirmButton: false,
                    timer: 2000,
                    text: 'Pembayaran anda gagal !',

                })
            }
        })
    });
</script>
@endisset
@endsection
