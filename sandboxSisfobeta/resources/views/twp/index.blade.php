@extends('layouts.base')


@section('content')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-list"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>E-Twp</h3>
        <p>API EKpr adalah api untuk mengakses informasi pinjaman rumah para prajurit</p>
    </div>

    <div class="page-content">
        <h1 class="SubContentName" id="dokumentasi">
            <a href="#dokumentasi">Dokumentasi</a>
        </h1>
        <div class="contentPart">
            <h4>Versi</h4>
            <table>
                <thead>
                    <tr>
                        <th>Versi</th>
                        <th>Tanggal</th>
                        <th>Perubahan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><code class="highlighter-rouge">versi 1</code></td>
                        <td>04/10/2021</td>
                        <td>Initial deployment</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="contentPart">
            <h4>Endpoints</h4>
            <table>
                <thead>
                    <tr>
                        <th>Endpoint</th>
                        <th>Kegunaan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><code class="highlighter-rouge">/baltab/sanbox/calldatakeluar</code></td>
                        <td>Return data sandbox riwayat hidup prajurit baltab</td>
                    </tr>

                </tbody>
            </table>
        </div>

        <h1 class="SubContentName" id="reqParameter">
            <a href="#reqParameter">Required Parameter</a>
        </h1>
        <div class="contentPart">
            <h4>Example API Fields</h4>
            <table>
                <thead>
                    <tr>
                        <th>Parameter</th>
                        <th>Deskripsi</th>
                        <th>Tipe Data</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><code class="highlighter-rouge">TOKEN</code></td>
                        <td>Token untuk access API</td>
                        <td>string</td>
                    </tr>

                </tbody>
            </table>
        </div>
        <pre class=" language-bash" tabindex="0">
            <code class=" language-bash">
                <span class="token operator">"0": {</span>

             <span class="token function"> "atas_nama": "AGS",
                "jumlah": "3213213",
                "total_bulan": "12",
                "nama_bank": "BRI",
                "no_rekening": "4324244",
                "data_prajurit": {
                    "nama_prajurit": "Agus",
                    "nrp": "333",
                    "tanggal pensiun": "2034-08-13"
                }</span>
              <span class="token operator"> },</span>
            </code>
        </pre>
    </div>
    

    <footer>
        <div class="footer clearfix mb-0 text-muted">
            <div class="d-flex justify-content-center">
                <p>2021 &copy; ETWPAD</p>
            </div>
        </div>
    </footer>
</div>
@endsection