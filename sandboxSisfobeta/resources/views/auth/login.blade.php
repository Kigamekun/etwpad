<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | SISFOBETA</title>
    <!--Bootstrap Asset-->
    <link rel="stylesheet" href="{{ url ('baseassets/vendors/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!--Dashboard Css-->
    <link rel="stylesheet" href="{{ url('baseassets/css/login.css') }}">

    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">

</head>
<body>

    <section class="authLogin">

        <div class="wrapperAuth">
            <div class="leftBackground">
                <div class="gradient">
                    <div class="authInformation">
                        <div class="title">
                            <h1>SISFOBETA <span class="version">4.0</span></h1>
                            <p>SISTEM INFORMASI BENDAHARA TWP AD ONLINE</p>
                        </div>
                        <div class="paragrafInfo">
                            <p>Suatu aplikasi yg memudahkan setiap prajurit di seluruh penjuru Indonesia dalam mengakses baltab, iuran twp dan pengajuan serta transaksi KPR</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rightContent">
                <div class="headingAuth">
                    <div class="textWelcome">
                        <h1 class="welcome">SELAMAT DATANG DI</h1>
                        <h1 class="nameApps">SANDBOX ETWPAD <span class="version">4.0</span></h1>
                    </div>
                    <div class="patners">
                        <div class="tni_ad">
                            <img src="{{ url('baseassets/img/tni-ad.svg') }}" alt="" style="max-width: 60px;min-width: 60px;">
                        </div>
                        <div class="bank-btn">
                            <!-- <img src="{{ url('baseassets/img/bankBTN.svg') }}" alt=""> -->
                            <img src="https://i.ibb.co/KxsBZk9/photo-2021-10-06-15-38-20-removebg-preview.png" style="max-width: 120px;min-width: 120px;" alt="">
                        </div>
                    </div>
                </div>
                @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
                <div class="mainAuth">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="nrpInput inputSection">
                            <label for="nrp">Email</label>
                            <div class="icon-control">
                                <img src="{{ url('baseassets/img/nrp.svg') }}" alt="">
                            </div>
                            <input name="email" class="form-control nrp" type="text" placeholder="Masukan E-Mail anda" autocomplete="off" required>
                        </div>
                        <div class="passwordInput inputSection">
                            <label for="password">Password</label>
                            <div class="icon-control">
                                <img src="{{ url('baseassets/img/key.svg') }}" alt="">
                            </div>
                            <input name="password" class="form-control password" type="password" placeholder="Masukan password anda" id="password" autocomplete="new-password" required>
                            <i class="bi bi-eye-slash eyesPassword" id="togglePassword"></i>
                        </div>
                        <div class="confirmation-user d-flex align-items-center justify-content-between">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                 Ingat saya
                                </label>
                            </div>
                            {{-- @if (Route::has('password.request'))
                            <a class="forgetPassowrd" href="{{ route('forgotPassword') }}">Lupa Password</a>
                        @endif --}}
                        </div>
                        <button type="submit" style="color: white;font-weight: bold" class="btn login">Masuk</button>
{{--
                        <div class="toRegisterPage">
                            <p>Belum punya akun?  <a href="{{ route('register') }}">Daftar</a></p>
                        </div> --}}
                    </form>

                </div>
            </div>
        </div>

    </section>





    <!--jQuery-->
    <script src="../baseassets/vendors/jquery/jquery.min.js"></script>
    <!--Bootstrap Asset-->
    <script src="../baseassets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!--Ion Icon-->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

    <script>

        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);

            this.classList.toggle('bi-eye');
        });
    </script>
</body>
</html>
