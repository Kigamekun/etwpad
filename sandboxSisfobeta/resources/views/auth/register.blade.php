<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register | SISFOBETA</title>
    <!--Bootstrap Asset-->
    <link rel="stylesheet" href="{{ url ('baseassets/vendors/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!--Dashboard Css-->
    <link rel="stylesheet" href="{{ url('baseassets/css/register.css') }}">

    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">
</head>
<body>

    <section class="authRegister">

        <div class="wrapperAuth">
            <div class="leftBackground">
                <div class="gradient">
                    <div class="authInformation">
                        <div class="title">
                            <h1>SISFOBETA <span class="version">4.0</span></h1>
                            <p>SISTEM INFORMASI BENDAHARA TWP AD ONLINE</p>
                        </div>
                        <div class="paragrafInfo">
                            <p>Suatu aplikasi yg memudahkan setiap prajurit di seluruh penjuru Indonesia dalam mengakses baltab, iuran twp dan pengajuan serta transaksi KPR</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rightContent">
                <div class="headingAuth">
                    <div class="textWelcome">
                        <h1 class="welcome">SELAMAT DATANG DI</h1>
                        <h1 class="nameApps">SISFOBETA <span class="version">4.0</span></h1>
                    </div>
                    <div class="patners">
                        <div class="tni_ad">
                            <img src="{{url ('baseassets/img/tni_logo.png')}}" alt="">
                        </div>
                        <div class="bank-btn">
                            <!-- <img src="{{url ('baseassets/img/bankBTNLOGO.png')}}" alt=""> -->
                            <img src="https://i.ibb.co/KxsBZk9/photo-2021-10-06-15-38-20-removebg-preview.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="mainAuth">
                    <div class="notice">
                        <p>Silahkan daftar dan mengisi data dengan benar.</p>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('err'))
                <div style="margin-bottom: 0;" class="alert alert-warning">
                    <ul>

                            <li>{{ session('err') }}</li>

                    </ul>
                </div>
                @endif
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                       
                        <div class="nameInput inputSection ">
                            <div class="icon-control">
                                <img src="{{url ('baseassets/img/people.svg')}}" alt="">
                            </div>
                            <input name="name" class="form-control inputName" type="text" placeholder="Masukan nama anda" autocomplete="off" required>
                        </div>
                        <div class="emailInput inputSection">
                            <div class="icon-control">
                                <img src="{{url ('baseassets/img/email.svg')}}" alt="">
                            </div>
                            <input name="email" class="form-control" type="email" placeholder="Masukan email anda" autocomplete="off" required>
                        </div>
                        
                        <div class="passwordInput inputSection">
                            <div class="icon-control">
                                <img src="{{url ('baseassets/img/key.svg')}}" alt="">
                            </div>
                            <input name="password" class="form-control" type="password"  id="password" placeholder="Masukan password anda" autocomplete="new-password">
                            <i class="bi bi-eye-slash eyesPassword" id="togglePassword"></i>

                        </div>
                        <div class="confirmPassword inputSection">
                            <div class="icon-control">
                                <img src="{{url ('baseassets/img/key.svg')}}" alt="">
                            </div>
                            <input name="password_confirmation" class="form-control" type="password" id="ConfirmasiPassword" placeholder="konfirmasi password" autocomplete="off">
                            <i class="bi bi-eye-slash eyesPassword" id="toggleConfirmPassword"></i>
                        </div>
                        <button style="color: white;font-weight: bold" type="submit" class="btn register">Daftar</button>
                    </form>

                </div>
            </div>
        </div>


    </section>





    <!--jQuery-->
    <script src="{{ url ('baseassets/vendors/jquery/jquery.min.js') }}"></script>
    <!--Bootstrap Asset-->
    <script src="{{ url ('baseassets/vendors/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!--Ion Icon-->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>


    <script>

        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');
        const toggleConfirmPassword = document.querySelector('#toggleConfirmPassword');
        const confirmPassword = document.querySelector('#ConfirmasiPassword');

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            this.classList.toggle('bi-eye');
        });

        toggleConfirmPassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = confirmPassword.getAttribute('type') === 'password' ? 'text' : 'password';
            confirmPassword.setAttribute('type', type);
            this.classList.toggle('bi-eye');
        });

    </script>
</body>
</html>
