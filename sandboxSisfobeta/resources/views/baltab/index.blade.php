@extends('layouts.base')


@section('content')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-list"></i>
        </a>
    </header>

    <div class="page-heading">
        <h1>E-Baltab</h1>
        <p>Sandbox Ebaltab adalah api untuk mengakses informasi riwayat hidup prajurit TNI</p>
    </div>


    <footer>
        <div class="footer clearfix mb-0 text-muted">
            <div class="d-flex justify-content-center">
                <p>2021 &copy; ETWPAD</p>
            </div>
        </div>
    </footer>
</div>
@endsection
