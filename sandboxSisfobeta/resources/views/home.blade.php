@extends('layouts.base')


@section('content')
<div id="main">
    @if(session('message'))
    <div class="alert alert-success" role="alert">
        {{ session('message') }}
</div>

    @endif
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-list"></i>
        </a>
    </header>

    <div class="page-heading">
        <h1>PENDAHULUAN</h1><br>
<h3>Selamat datang di sandbox ETWPAD</h3>
<p>Sandbox ETWPAD adalah website sandbox yang berfungsi sebagai wadah pengujian API dan koneksi ke ETWPAD.<br>
Ini membantu Anda dengan mudah menerima dan testing API TWP, KPR, dan BALTAB.
Anda dapat memilih untuk berintegrasi dengan berbagai pilihan API yang disediakan oleh ETWPAD.
API kami yang sangat umum terstruktur untuk semua.
Setelah menguji integrasi ini, Anda dapat beralih ke Lingkungan Produksi.
Untuk detail lebih lanjut, lihat menu Api(E-Baltab, E-KPR, E-TWP).
Anda dapat menggunakan Lingkungan Sandbox untuk menguji integrasi Anda, sebelum melakukan aksi real-time.<br>
Setelah Anda puas dengan hasilnya, Anda dapat beralih ke Lingkungan Produksi.</p>

<br>

<h3>Base path API</h3>
<p>Base path untuk akses API ETWPAD </p><br>
<pre class=" language-bash" tabindex="0">
    <code class=" language-bash">
     <span class="token function"> https://asiabytes.tech/api
    </code>
</pre>
<br>

<h1> VERSI </h1>
                <p>Versi API saat ini adalah <span class="badge rounded-pill bg bg-primary" style="color : white">v1.0</span></p>
    </div>
<br>


    <br>
    <div class="page-content">
        <div class="d-flex">
            <div style="flex:1;">

            </div>
            <div style="flex:1;">

            </div>
        </div>
    </div>
    <br>
    <br>
    <br>

    <footer>
        <div class="footer clearfix mb-0 text-muted">
            <div class="d-flex justify-content-center">
                <p>2021 &copy; ETWPAD</p>
            </div>
        </div>
    </footer>
</div>

@endsection

