<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kpr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Transaksi;
use File;
use Illuminate\Support\Facades\Log;

class KprController extends Controller
{




    public function pinjaman(Request $request)
    {

    
        $kpr = Kpr::where('user_id',Auth::id())->first();

      

        if (is_null($kpr)) {
            $data = app('App\Http\Controllers\AnuitasController')->TabelAngsuran(Carbon::now()->toDateString('Y-m-d'),$request->harga,$request->jk_waktu);
            $pokok_bulan_ini = $data[0]['pokok'][1];
            $bunga_bulan_ini = $data[0]['bunga'][1];
            if (strlen(Auth::user()->norek) == 15 || strlen(Auth::user()->norek) == 10) {

            Kpr::create([
                'tmt_angsuran'=>now(),
                'nrp'=>Auth::user()->nrp,
                'nama'=>Auth::user()->name,
                'user_id'=>Auth::id(),
                'tahap'=>0,
                'pinjaman'=>$request->harga,
                'jk_waktu'=>$request->jk_waktu,
                'rekening'=>Auth::user()->norek,
                'nama_bank'=>'BRI',
    'angs_ke'=>0,

    'tunggakan'=>1,
    
                'pokok_bulan_ini'=>$pokok_bulan_ini,
                'bunga_bulan_ini'=>$bunga_bulan_ini,
    'angs_ke'=>1,
                'angs_masuk'=>0,
                'jml_tunggakan'=>$data[1],
                'jml_angs'=>$data[1],
                'bunga'=>array_sum($data[0]['bunga']),
                'pokok'=>array_sum($data[0]['pokok']),

                'rumah_id'=>$request->rumah_id,
                'status'=>1,
                'outstanding'=>$pokok_bulan_ini + $bunga_bulan_ini

            ]);
            }else if (strlen(Auth::user()->norek) == 16){

            Kpr::create([
                'tmt_angsuran'=>now(),
                'nrp'=>Auth::user()->nrp,
                'nama'=>Auth::user()->name,
                'user_id'=>Auth::id(),
                'tahap'=>0,
                'pinjaman'=>$request->harga,
                'jk_waktu'=>$request->jk_waktu,
                'rekening'=>Auth::user()->norek,
                'nama_bank'=>'BTN',
                'rumah_id'=>$request->rumah_id,
                'pokok_bulan_ini'=>$pokok_bulan_ini,
                'bunga_bulan_ini'=>$bunga_bulan_ini,
                'angs_ke'=>1,
                'angs_masuk'=>0,
                'jml_tunggakan'=>$data[1],
                'tunggakan'=>1,
                'jml_angs'=>$data[1],
                'bunga'=>array_sum($data[0]['bunga']),
                'pokok'=>array_sum($data[0]['pokok']),
                'status'=>3,
                'outstanding'=>$pokok_bulan_ini + $bunga_bulan_ini
            ]);

            }else {
                return redirect()->route('kpr.index')->with(['message'=>"No Rekening anda tidak valid harap perbaiki"]);
            }
            return redirect()->route('kpr.index')->with(['message'=>"Permintaan telah diajukan dan telah di approve admin"]);


        } else {
            return redirect()->route('kpr.index',['message'=>"Anda telah memiliki KPR"]);
        }


    }



    public function seePinjaman()
    {
        $kpr = Kpr::where('nrp',Auth::user()->nrp)->first();

        // dd($kpr);
        $waktu_selesai = date('Y-m-d', strtotime("+".$kpr->jk_waktu." months", strtotime($kpr->tmt_angsuran)));
        $tahun_selesai = explode('-',$waktu_selesai);

        $besar_pinjaman = $kpr->pinjaman;
        $bunga = 6/100;
        $jangka = $kpr->jk_waktu;
        $tahun = $kpr->jk_waktu/12;

        $c = pow((1 + $bunga), $tahun);
        $d = $c - 1;
        $fax = ($bunga * $c) / $d;

        //tergantung mau dibuletinnya gimana
        $anunitas = round($fax, 6);
        $besar_angsur = ($besar_pinjaman * $anunitas) / 12;
        $besar_angsuran = round($besar_angsur, -3) + 1000;


        $transaksi = Transaksi::where('nrp', Auth::user()->nrp)
        ->where('status', '0001')
        ->get();


        // dd($transaksi);
        $tahun_awal = date('Y', strtotime($kpr->tmt_angsuran));
        $years = [];
        for ($i = $tahun_awal; $i <= $tahun_selesai[0]; $i++) {
            $years[] = $i;
        }

        $trx = [];
        foreach ($transaksi as $key => $value) {
            $trx[] = date("Y-m", strtotime($value->tanggal));
        }

        $akhir = date('Y-m', strtotime("+".$kpr->jk_waktu." months", strtotime($kpr->tmt_angsuran)));
        $years = array_values(array_reverse(array_map('strval', $years), true));


       

    }



    private function refresh_saldo($id)
    {
        $kpr = Kpr::find($id);

        $besar_pinjaman = $kpr->pinjaman;
        $bunga = 6;
        $jangka = $kpr->jk_waktu;

        $bungapersen = $bunga / 100;
        $tahun = $jangka / 12;

        $c = pow((1 + $bungapersen), $tahun);
        $d = $c - 1;
        $fax = ($bungapersen * $c) / $d;
        $anuitas = round($fax, 6);

        $besar_angsur = ($besar_pinjaman * $anuitas) / 12;

        $besar_angsuran = round($besar_angsur, -3);
        if ($besar_angsuran != $kpr->jml_angs) {
            $besar_angsuran = round($besar_angsur, -3) + 1000;
        }

        $array1 = [0 => null];
        $array2 = [0 => null];
        $array3 = [0 => intval($besar_pinjaman)];
        $array4 = [0 => 0];

        $array_utang1 = [0 => null];
        $array_utang2 = [0 => null];
        $array_utang3 = [0 => intval($besar_pinjaman)];
        $array_utang4 = [0 => 0];

        $no = 1;
        $b = 1;
        $angsuran_bunga = $besar_pinjaman * $bungapersen / 12;
        $angsuran_pokok = $besar_angsuran - $angsuran_bunga;

        $angsuran_bunga_utang = $besar_pinjaman * $bungapersen / 12;
        $angsuran_pokok_utang = $besar_angsuran - $angsuran_bunga;

        $bulan = (int)date('m', strtotime($kpr->tmt_angsuran));

        $sisa_angsuran = $kpr->jk_waktu - $kpr->angsuran_masuk - $kpr->tunggakan;

        for ($i = $bulan; $i < $kpr->angsuran_masuk + $bulan; $i++) {

            if ($i == 13 || $no == 13) {
                $ang_bunga = $besar_pinjaman * $bungapersen / 12;
                $angsuran_bunga = round($ang_bunga);
                $angsuran_pokoks = $besar_angsuran - $angsuran_bunga;
                $angsuran_pokok = round($angsuran_pokoks);
                $no = 1;
            }

            $no++;
            array_push($array1, $angsuran_bunga);
            array_push($array2, $angsuran_pokok);
            array_push($array4, $besar_angsuran);

            $besar_pinjaman -= $array2[$b];
            $b++;
            array_push($array3, $besar_pinjaman);
        }

        $saldo_bunga_total = array_sum(array_slice($array1, 0, $kpr->angsuran_masuk+1));
        $saldo_pokok_total = array_sum(array_slice($array2, 0, $kpr->angsuran_masuk+1));

        $utang_bunga_total = array_sum(array_slice($array1, $kpr->angsuran_masuk + 1, $jangka));
        $utang_pokok_total = array_sum(array_slice($array2, $kpr->angsuran_masuk + 1, $jangka));

        return [
            "saldo_pokok" => $saldo_pokok_total,
            "saldo_bunga" => $saldo_bunga_total,
        ];
    }





 public function refreshData()
    {

       
        $su = 0;
        Kpr::where('status', '<', '4')->with(['trx_bri' => function($q) {
            $q->where('status', '0001')->orderByDesc('tanggalproses');
        }])
            ->each(function ($kpr) {
                if($kpr->jk_waktu != $kpr->angs_ke){
                    try {
                        $trx = $kpr->trx_bri->first();
                        $tunggakan = $kpr->tunggakan;
                        $lastbayar=date_create();

                        $Hasil = Transaksi::where('status','0001')->get()->unique('tanggal')->count();
                       
                        $tmt=date_create(substr($kpr->tmt_angsuran,0,7));
                        $diff=date_diff($tmt,$lastbayar);
                        $semuabulan = ($diff->y * 12) + $diff->m + 1;
                        $tunggakan = $semuabulan - $Hasil;
                        if ($tunggakan <= 0) {
                            $tunggakan = 0;
                        }

                        $jml_tunggakan = $kpr->jml_angs * $tunggakan;
                        $angs_ke = ($diff->y * 12) + $diff->m + 1;
                            $kpr->update([
                            
                            'angsuran_masuk'=>$Hasil,
                            'angs_ke' => $angs_ke,
                            'tunggakan' => $tunggakan,
                            'jml_tunggakan' => $jml_tunggakan,
                            ]);
                            $bunga = 6;
                            $jangka = $kpr->jk_waktu;

                            $bln = explode('-',$kpr->tmt_angsuran);
                            $bln = intval($bln[1]);
                            $data = app('App\Http\Controllers\rumusController')->TabelAngsuran($bln, $kpr->pinjaman, $kpr->jk_waktu);
                            $saldo_bunga_total = array_sum(array_slice($data[0]['bunga'], 0, $kpr->angsuran_masuk+1));
                            $saldo_pokok_total = array_sum(array_slice($data[0]['pokok'], 0, $kpr->angsuran_masuk+1));

                            $pokok_bulan_ini = $data[0]['pokok'][$kpr->angs_ke];
                            $bunga_bulan_ini = $data[0]['bunga'][$kpr->angs_ke];
                    
                            // isinya array
                            $refresh_saldo = $this->refresh_saldo($kpr->id);
                            $refresh_piutang = $this->refresh_piutang($kpr->id);
                        
                            $kpr->update([
                                'bunga' => $saldo_bunga_total,
                                'pokok' => $saldo_pokok_total,
                                'piutang_bunga' => $refresh_piutang['utang_bunga'],
                                'piutang_pokok' => $refresh_piutang['utang_pokok'],
                                'tunggakan_pokok' => $refresh_piutang['tunggakan_pokok'],
                                'tunggakan_bunga' => $refresh_piutang['tunggakan_bunga'],
                                'pokok_bulan_ini' => $pokok_bulan_ini,
                                'bunga_bulan_ini' => $bunga_bulan_ini,
                            ]);
            

                    } catch (\Exception $e) {
                        dd($e);
                    }
                } else if ($kpr->jk_waktu == $kpr->angs_ke &&  $kpr->angsuran_masuk == $kpr->angs_ke){
                    try {
                        DB::transaction(function () use ($kpr) {
                            $kpr->update([
                                'status' => 4,
                            ]);
                        });
                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
                    }
                }
            }
        );
        Log::info("Update berhasil");
      



    }



    public function bayarPost(Request $request)
    {

        $kpr = Kpr::where('user_id',Auth::id())->first();
        $input = $request->all();
        $input['data'] = $request->data;


        if ($input['data'] == null) {
            Alert::warning(
                'Tidak ada bulan yang dipilih',
                'Jika ingin Membayar maka pilihlah'
            );
            return back();
        }

        try {

                if ($kpr->rek_bri) {
                    $rekening = $kpr->rek_bri;
                } else if($kpr->rek_bri == null && $kpr->rek_btn) {
                    $rekening = $kpr->rek_btn;
                } else if($kpr->rek_bri == null && $kpr->rek_btn == null && $kpr->rekening) {
                    $rekening = $kpr->rekening;
                } else {
                    $rekening = "0000000000000";
                }

            // Api BTN Logic Start

                foreach ($input['data'] as $tahun => $bulan) {
                    foreach ($bulan as $bln) {
                        $month = $bln < 10 ? "0{$bln}" : $bln;
                        $tanggal = "{$tahun}-{$month}-01";

                    Transaksi::create([
                            'nrp' => $kpr->nrp,
                            'kodecabang' => '0018',
                            'tanggal' => $tanggal,
                            'rekeningdebet' => $rekening,
                            'matauang' => 'IDR',
                            'tanggalproses' => $tanggal . " " . "01:00:00",
                            'amountdebet' => $kpr->jml_angs,
                            'status' => '0001',
                            'deskripsi' => 'Transaction Successful',
                            'nama' => $kpr->nama,
                        ]);
                    }

                    foreach ($bulan as $bln) {
                        // isinya array
                        $refresh_saldo = $this->refresh_saldo($kpr->id);
                        $refresh_piutang = $this->refresh_piutang($kpr->id);

                        $month = $bln < 10 ? "0{$bln}" : $bln;
                        $tanggal = "{$tahun}-{$month}-01";


                        if ($kpr->tunggakan > 0) {
                            $angsuran_masuk = $kpr->angsuran_masuk + 1;
                            $tunggakan = $kpr->tunggakan - 1;
                            $jml_tunggakan = $kpr->jml_tunggakan - $kpr->jml_angs;

                            $kpr->update([
                                'angsuran_masuk' => $angsuran_masuk,
                                'tunggakan' => $tunggakan,
                                'jml_tunggakan' => $jml_tunggakan,
                            ]);
                        } else {
                            $angs_ke = $kpr->angs_ke + 1;
                            $angsuran_masuk = $kpr->angsuran_masuk + 1;

                            $kpr->update([
                                
                                'angsuran_masuk' => $angsuran_masuk,
                            ]);
                        }



                        $kpr->update([
                            'bunga' => $refresh_saldo['saldo_bunga'],
                            'pokok' => $refresh_saldo['saldo_pokok'],
                            'piutang_bunga' => $refresh_piutang['utang_bunga'],
                            'piutang_pokok' => $refresh_piutang['utang_pokok'],
                        ]);
                    }
                }
            // });


            // End logic API



                $this->refreshData();
            return back()->with('message','berhasil membayar');
        } catch (\Exception $e) {
            
            return back()->with('message', $e->getMessage());
        }





    return response()->json(['message'=>'success'], 200);
    }


    private function refresh_piutang($id)
    {
        $kpr = Kpr::find($id);
        
        // variable yang dibutuhkan
        $besar_pinjaman = [0 => 'besar pinjaman'];
        $jangka = [0 => 'jangka'];
        $angsuran_masuk = [0 => 'angsuran masuk'];
        $sisa_angsuran = [0 => 'sisa angsuran'];
        $tunggakan = [0 => 'tunggakan'];
        $id = [];
        $bulan = [0 => 'tmt_angsuran'];

        $bunga = 6;
        $array_orang_pokok = [];
        $array_orang_bunga = [];

        $tunggakan_pokok = [0 => 'tunggakan pokok'];
        $tunggakan_bunga = [0 => 'tunggakan bunga'];

        array_push($tunggakan, $kpr->tunggakan);
        array_push($id, $kpr->id);
        array_push($besar_pinjaman, $kpr->pinjaman);
        array_push($jangka, $kpr->jk_waktu);
        array_push($angsuran_masuk, $kpr->angsuran_masuk);
        array_push($sisa_angsuran, ($kpr->jk_waktu - $kpr->angsuran_masuk) + $kpr->tunggakan);

        for ($index = 1; $index <= count($id); $index++) {

            //tentuin bunga ke persen 6 ke 6%
            $bungapersen = $bunga / 100;

            // tentuin tahun dari jangka
            $tahun = $jangka[$index] / 12;

            // ===>mencari anuitas<===
            $c = pow((1 + $bungapersen), $tahun);
            $d = $c - 1;
            $fax = ($bungapersen * $c) / $d;
            $anuitas = round($fax, 6);
            // ===>mencari anuitas<===

            $besar_angsur = ($besar_pinjaman[$index] * $anuitas) / 12;
            $besar_angsuran = round($besar_angsur, -3);
            if ($besar_angsuran != $kpr->jml_angs) {
                $besar_angsuran = round($besar_angsur, -3) + 1000;
            }

            $array_bunga = [0 => 'bunga'];
            $array_pokok = [0 => 'pokok'];
            $array_pinjaman = [0 => 'pinjaman'];

            $no = 1;
            $angsuran_bunga = $besar_pinjaman[$index] * $bungapersen / 12;
            $angsuran_pokok = $besar_angsuran - $angsuran_bunga;

            $bulan = (int)date('m', strtotime($kpr->tmt_angsuran));
            $b = 1;

            for ($i = $bulan; $i < $jangka[$index] + $bulan; $i++) {

                if ($i == 13 || $no == 13) {
                    $ang_bunga = $besar_pinjaman[$index] * $bungapersen / 12;
                    $angsuran_bunga = round($ang_bunga);
                    $angsuran_pokoks = $besar_angsuran - $angsuran_bunga;
                    $angsuran_pokok = round($angsuran_pokoks);
                    $no = 1;
                }

                $no++;
                array_push($array_bunga, $angsuran_bunga);
                array_push($array_pokok, $angsuran_pokok);

                $besar_pinjaman[$index] -= $array_pokok[$b];
                $b++;
                array_push($array_pinjaman, $besar_pinjaman);
            }

            $utang_bunga_total = array_sum(array_slice($array_bunga, $angsuran_masuk[$index] + 1, $jangka[$index]));
            $utang_pokok_total = array_sum(array_slice($array_pokok, $angsuran_masuk[$index] + 1, $jangka[$index]));

            array_push($array_orang_pokok, $utang_pokok_total);
            array_push($array_orang_bunga, $utang_bunga_total);
            
            //CARI TUNGGAKAN POKOK DAN BUNGA
            $tunggakan_pokok_orang = array_sum(array_slice($array_pokok, $angsuran_masuk[$index] + 1, $tunggakan[$index]));
            $tunggakan_bunga_orang = array_sum(array_slice($array_bunga, $angsuran_masuk[$index] + 1, $tunggakan[$index]));
            array_push($tunggakan_pokok, $tunggakan_pokok_orang);
            array_push($tunggakan_bunga, $tunggakan_bunga_orang);
        }

        return [
            "utang_pokok" => $array_orang_pokok[0],
            "utang_bunga" => $array_orang_bunga[0],
            'tunggakan_pokok' => $tunggakan_pokok[1],
            'tunggakan_bunga' => $tunggakan_bunga[1]
        ];
    }

    public function store(Request $request, $id)
    {
        $kpr = Detailkpr::find($id);
        $currentYear = date('Y');
        $input = $request->all();
        $input['data'] = $request->data;

        if ($input['data'] == null) {
            Alert::warning(
                'Tidak ada bulan yang dipilih',
                'Jika ingin mengupdate, maka Anda harus memilih bulan yang akan diupdate'
            );
            return back();
        }

        try {
            DB::transaction(function() use ($id, $input, $currentYear, $kpr) {        
                if ($kpr->rek_bri) {
                    $rekening = $kpr->rek_bri;
                } else if($kpr->rek_bri == null && $kpr->rek_btn) {
                    $rekening = $kpr->rek_btn;
                } else if($kpr->rek_bri == null && $kpr->rek_btn == null && $kpr->rekening) {
                    $rekening = $kpr->rekening;
                } else {
                    $rekening = "0000000000000";
                }


                foreach ($input['data'] as $tahun => $bulan) {
                    $thn = $tahun;

                    foreach ($bulan as $bln) {
                        $month = $bln < 10 ? "0{$bln}" : $bln;
                        $tgl = $thn . "-" . $month . "-01";
                        $tanggalproses = date('Y-m-d 01:i:s', strtotime($tgl));
                        $tanggal = date('Y-m-d', strtotime($tgl));
                        $tmt_angsuran = date('n Y', strtotime($kpr->tmt_angsuran));
                        $now = date('n Y');
                        
                        if ($kpr->tunggakan > 0) {
                            $angsuran_masuk = $kpr->angsuran_masuk + 1;
                            
                            if (strtotime($tgl) > strtotime(date('Y-m-01'))){
                                $tunggakan = $kpr->tunggakan;
                                $jml_tunggakan = $kpr->jml_tunggakan;
                            } else {
                                $tunggakan = $kpr->tunggakan - 1;
                                $jml_tunggakan = $kpr->jml_tunggakan - $kpr->jml_angs;
                            }

                            $kpr->update([
                                'angsuran_masuk' => $angsuran_masuk,
                                'tunggakan' => $tunggakan,
                                'jml_tunggakan' => $jml_tunggakan,
                            ]);
                        } else {
                            $angsuran_masuk = $kpr->angsuran_masuk + 1;
                            
                            $kpr->update([
                                // 'angs_ke' => $angs_ke,
                                'angsuran_masuk' => $angsuran_masuk,
                            ]);
                        }

                        $besar_pinjaman = $kpr->pinjaman;
                        $bunga = 6;
                        $jangka = $kpr->jk_waktu;
                
                        $bungapersen = $bunga / 100;
                        $tahun = $jangka / 12;
                
                        $c = pow((1 + $bungapersen), $tahun);
                        $d = $c - 1;
                        $fax = ($bungapersen * $c) / $d;
                        $anuitas = round($fax, 6);
                
                        $besar_angsur = ($besar_pinjaman * $anuitas) / 12;
                
                        $besar_angsuran = round($besar_angsur, -3);
                        if ($besar_angsuran != $kpr->jml_angs) {
                            $besar_angsuran = round($besar_angsur, -3) + 1000;
                        }
                
                        $array1 = [0 => null];
                        $array2 = [0 => null];
                        $array3 = [0 => intval($besar_pinjaman)];
                        $array4 = [0 => 0];
                
                        $array_utang1 = [0 => null];
                        $array_utang2 = [0 => null];
                        $array_utang3 = [0 => intval($besar_pinjaman)];
                        $array_utang4 = [0 => 0];
                
                        $no = 1;
                        $b = 1;
                        $angsuran_bunga = $besar_pinjaman * $bungapersen / 12;
                        $angsuran_pokok = $besar_angsuran - $angsuran_bunga;
                
                        $angsuran_bunga_utang = $besar_pinjaman * $bungapersen / 12;
                        $angsuran_pokok_utang = $besar_angsuran - $angsuran_bunga;
                
                        $bulan = (int)date('m', strtotime($kpr->tmt_angsuran));
                
                        $sisa_angsuran = $kpr->jk_waktu - $kpr->angsuran_masuk - $kpr->tunggakan;
                
                        for ($i = $bulan; $i < $kpr->angsuran_masuk + $bulan; $i++) {
                
                            if ($i == 13 || $no == 13) {
                                $ang_bunga = $besar_pinjaman * $bungapersen / 12;
                                $angsuran_bunga = round($ang_bunga);
                                $angsuran_pokoks = $besar_angsuran - $angsuran_bunga;
                                $angsuran_pokok = round($angsuran_pokoks);
                                $no = 1;
                            }
                
                            $no++;
                            array_push($array1, $angsuran_bunga);
                            array_push($array2, $angsuran_pokok);
                            array_push($array4, $besar_angsuran);
                
                            $besar_pinjaman -= $array2[$b];
                            $b++;
                            array_push($array3, $besar_pinjaman);
                        }
                
                        $saldo_bunga_total = array_sum(array_slice($array1, 0, $kpr->angsuran_masuk+1));
                        $saldo_pokok_total = array_sum(array_slice($array2, 0, $kpr->angsuran_masuk+1));
                        
                        // isinya array
                        $refresh_saldo = $this->refresh_saldo($id);
                        $refresh_piutang = $this->refresh_piutang($id);

                        $kpr->update([
                            'bunga' => $saldo_bunga_total,
                            'pokok' => $saldo_pokok_total,
                            'piutang_bunga' => $refresh_piutang['utang_bunga'],
                            'piutang_pokok' => $refresh_piutang['utang_pokok'],
                        ]);

                        if ($kpr->tunggakan > 0) {
                            if (strtotime($tgl) < strtotime(date('Y-m-01'))){
                                $kpr->update([
                                    'tunggakan_pokok' => $refresh_piutang['tunggakan_pokok'],
                                    'tunggakan_bunga' => $refresh_piutang['tunggakan_bunga']
                                ]);
                            }
                        } else {
                            $kpr->update([
                                'tunggakan_pokok' => 0,
                                'tunggakan_bunga' => 0
                            ]);
                        }
                        
                        $transaksi = $this->transaksi->create([
                            'nrp' => $kpr->nrp,
                            'kodecabang' => '0018',
                            'tanggalproses' => $tanggalproses,
                            'matauang' => 'IDR',
                            'tanggal' => $tanggal,
                            'rekeningdebet' => $rekening,
                            'amountdebet' => $kpr->jml_angs,
                            'status' => '0001',
                            'deskripsi' => 'Transaction Successful',
                            'nama' => $kpr->nama,
                        ]);
                    }
                }
            });

            Alert::success('Informasi Pesan', 'Berhasil diupdate');
            return back();
        } catch (\Exception $e) {
            Alert::error('Informasi Pesan', "Gagal diupdate ". $e->getMessage());
            return back()->with('error_update', $e->getMessage());
        }
    }
    




}