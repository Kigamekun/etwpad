<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\potongan;
use App\Models\bunga;
class rumusController extends Controller
{
    public static function loadPeriod($period, $pangkat = '')
    {
        $period = join('-', explode('-', $period));
        if ($period >= '2009-04') {
            $find = DB::table('potongans')->where('period', '<=', $period)->where('pangkat', '*')->orderBy('period', 'desc')->first();
            return $find->value;
        } else {
            $find_query = ['pangkat'=>$pangkat];
            $find = Potongan::where($find_query)->first();
        }

        if ($find) {
            return $find['value'];
        }

        return 0;
    }


    public function cariTab($nrp)
    {
        $user=DB::table('datapokok')->where('nrp', $nrp)->first();
        // 3900152380269 , 620134 ,29593


        if (is_null($user)) {
            return response()->json(['message'=>'user tidak ditemukan'], 200);
        }else if(is_null($user->tmt_henti)){
            if (is_null($user->tgl_pensiun)) {
                $user->tmt_henti = date("Y-m-d");
            }else {
                $user->tmt_henti = $user->tgl_pensiun;
            }
            // return response()->json(['message'=>'TMT henti tidak didefinisikan'], 200);
        }
        $tmt_1 = $user->tmt_1 == '000000' ? '' : $user->tmt_1;
        $tmt_2 = $user->tmt_2 == '000000' ? '' : $user->tmt_2;
        $tmt_3 = $user->tmt_3 == '000000' ? '' : $user->tmt_3;
        $tmt_4 = $user->tmt_4 == '000000' ? '' : $user->tmt_4;
        $tmt_5 = $user->tmt_5 == '000000' ? '' : $user->tmt_5;
        $tmtrange=[$user->tmt_1,$user->tmt_2,$user->tmt_3,$user->tmt_4,$user->tmt_5,$user->tmt_henti];
        $pangkat=['tamtama','bintara','pama', 'pamen','pati','henti'];
        $panggkatbin=[];
        foreach ($tmtrange as $key=>$dam) {
            if (empty($dam)) {
                unset($tmtrange[$key]);
            } else {
                $panggkatbin[$key]=$pangkat[$key];
            }
        }

        $panggkatbin=array_values($panggkatbin);
        $tmtrange=array_values($tmtrange);

        $bunga_list = [];
        foreach (DB::table('bungas')->get() as $key => $value) {
            $bunga_list[$value->period] = $value->value;
        }

try {
    $dt1 = explode("-", $tmtrange[0]);
    $dat1 = $dt1[0]."-".$dt1[1]."-01";

    $dt2 = explode("-", $user->tmt_henti);

    $akh = $dt2[1]+1;


    if (strlen($akh) != 2) {
        $dat2 = $dt2[0]."-0".$akh."-01";
    }else {
        if ($akh > 12) {
            $dv = $dt2[0]+1;
            $dat2 = $dv."-01-01";
        }else {
            $dat2 = $dt2[0]."-".$akh."-01";
        }
    }

    $date1 = date_create($dat1);
    $date2 = date_create($dat2);
    $interval = $date1->diff($date2);
    $all = $interval->m + $interval->y * 12;
    $bunga =  DB::table('bungas')->where('period','<=',$dt1[0]."-".$dt1[1])->orderBy('period','DESC')->first()->value;


} catch (\Throwable $th) {
    return redirect()->back()->with(['message'=>'Error pada data itu']);
}

        if ($dt1[0].'-'.$dt1[1]<'1986-02') {
            $potongan=0;
        }
        else{
            $potongan =$this->loadPeriod(substr($tmtrange[0], 0, 7), $panggkatbin[0]);
        }

        $jml_tabungan = 0;
        $bunga_bulanan = 0;
        $bunga_arr = [];
        $tab_arr = [];
        $pangkat = '';
        $arr_pangkat = [];

        $pangkat_list = [];

        $dt = strtotime($dt1[0].'-'.$dt1[1]);

        $daass=[];
        for ($i=0; $i < $all; $i++) {
            if ($i == 0) {
                $jml_tabungan += $potongan;
            } else {
                $dn = date("Y-m", strtotime("+1 month", $dt));


                switch ($dn) {
            case substr($tmt_1, 0, 7):
                $pangkat = 'tamtama';
                break;
            case substr($tmt_2, 0, 7):
                $pangkat = 'bintara';
                break;
            case substr($tmt_3, 0, 7):
                $pangkat = 'pama';
                break;
            case substr($tmt_4, 0, 7):
                $pangkat = 'pamen';
                break;
            case substr($tmt_5, 0, 7):
                $pangkat = 'pati';
                break;
        }

        if ($dn == "1986-02") {
            $potongan = Potongan::where(['pangkat'=>$panggkatbin[0],'period'=>"1986-02"])->first()->value;

        }
        try {
            if (!is_null($bunga_list[$dn])) {
                $bunga = $bunga_list[$dn];
            }
        } catch (\Throwable $th) {

        }

        if ($dn == "2009-04" || $dn == "2017-01" ) {
            $potongan = $this->loadPeriod($dn, $pangkat);
                    $tab_arr[] = $jml_tabungan - array_sum($bunga_arr) - array_sum($tab_arr);
                } elseif (!in_array($pangkat, $pangkat_list)) {
                    if ($this->loadPeriod($dn, $pangkat) != 0) {
                        $potongan = $this->loadPeriod($dn, $pangkat);
                        $pangkat_list[] = $pangkat;
                        $tab_arr[] = $jml_tabungan - array_sum($bunga_arr) - array_sum($tab_arr);
                    }
                }
                // dump("jumlah tabungan ".$jml_tabungan ."pada tanggal ".$dn." pangkatnya ".$pangkat. " potongannya ".$potongan.'bunganya '.$bunga.' jml_bunga'.array_sum($bunga_arr));

                $bunga_bulanan = number_format(($jml_tabungan * ($bunga) / 12/100), 2, '.', '');
                $jml_tabungan = number_format(($jml_tabungan + ($jml_tabungan * ($bunga) / 12/100) + $potongan), 2, '.', '');
                $bunga_arr[] = (float) $bunga_bulanan;

                $dt = strtotime($dn);
            }
        }


        $hasil =[
            'bulan'=>$all,
            'jumlah'=>$jml_tabungan,
            'bunga'=>array_sum($bunga_arr)
        ];
        // die;
        return $hasil;
    }

    public function TabelAngsuran($bln,$pinjaman,$jk_waktu,$bunga=6)
    {
        $besar_pinjaman = $pinjaman;
        $bunga = $bunga/100;
        $jangka = $jk_waktu;

        $tahun = $jk_waktu/12;

        $c = pow((1 + $bunga), $tahun);
        $d = $c - 1;
        $fax = ($bunga * $c) / $d;

        //tergantung mau dibuletinnya gimana
        $anunitas = round($fax, 6);
        $besar_angsur = ($besar_pinjaman * $anunitas) / 12;
        $besar_angsuran = round($besar_angsur, -3) + 1000;
        // angsuran bunga
        $bunga_all = [0 => null];
        // angsuran pokok
        $pokok_all = [0 => null];
        // angsuran pinjaman
        $pinjaman_all = [0 => intval($besar_pinjaman)];

         $angsuran_bunga = $besar_pinjaman * $bunga / 12;
        $angsuran_pokok = $besar_angsuran - $angsuran_bunga;



        $ang = $bln;

        for ($i = 1; $i < $jangka + 1; $i++) {
            if($bln == 13){
              $ang_bunga = $besar_pinjaman * $bunga / 12;
                $angsuran_bunga = round($ang_bunga, 2);
                $angsuran_pokok = $besar_angsuran - $angsuran_bunga;
                $bln = 1;
            }

            $bunga_all[] = $angsuran_bunga;
            $pokok_all[] = $angsuran_pokok;
            $besar_pinjaman -= $pokok_all[$i];



            $pinjaman_all[] = $besar_pinjaman;
            $bln++;
        }

        $array_all = [
            'bunga' => $bunga_all,
            'pokok' => $pokok_all,
            'pinjaman' => $pinjaman_all,
        ];

        return [$array_all,$besar_angsuran];


    }


}
