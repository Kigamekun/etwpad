<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'TrxBri';
    protected $fillable = ['nrp','kodecabang','tanggal','rekeningdebet','matauang','tanggalproses','amountdebet','status','deskripsi','nama'];
    public function riwayat()
    {
        return $this->belongsTo(Riwayat::class,'nrp','nrp');
    }
    
      public function kpr()
    {
        return $this->belongsTo(Detailkpr::class, 'nrp', 'nrp');
    }
    use HasFactory;
}
