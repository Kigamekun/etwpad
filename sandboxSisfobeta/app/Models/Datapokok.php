<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datapokok extends Model
{
    protected $table = 'datapokok';
    protected $fillable = ['user_id','nrp','nama','tmt_1','tmt_2','tmt_3','tmt_4','tmt_5','tmt_henti','tgl_pensiun','tg_lahir','norek','narek','nabank'];
    use HasFactory;
}
