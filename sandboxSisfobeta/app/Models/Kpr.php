<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kpr extends Model
{
    protected $table = 'kpr';
    protected $fillable = ['nama','rumah_id','pangkat','user_id','nrp','kesatuan','alamat','kotama','corps','tahap','pinjaman','tmt_angsuran' ,'jk_waktu','jml_angsuran','jml_angs','angs_ke','angsuran_masuk','tunggakan','jml_tunggakan','tunggakan_pokok','tunggakan_bunga','sisa_pinjaman_pokok','keterangan','bunga','pokok','piutang_pokok', 'piutang_bunga','status','pokok_bulan_ini','bunga_bulan_ini','nama_bank'];
   

    public function getStatusPinjamAttribute(){
        return $this->status == null || $this->status == 0  || $this->status == 2 ? '<span class="badge badge-danger">BELUM APPROVAL</span>' : '<span class="badge badge-success">SUDAH APPROVAL</span>';
    }
      public function trx_bri()
    {
        return $this->hasMany(Transaksi::class, 'nrp', 'nrp');
    }
    use HasFactory;
}
