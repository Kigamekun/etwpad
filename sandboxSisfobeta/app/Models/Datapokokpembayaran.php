<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datapokokpembayaran extends Model
{
    protected $table = 'datapokok_pembayaran';
    protected $fillable = ['status'];
    use HasFactory;
}
