<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Kpr;
use App\Models\Baltab;
use App\Models\Transaksi;
use App\Models\Datapokok;
use App\Models\Datapokokpembayaran;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::middleware(['useToken'])->group(function () {

Route::prefix('kpr')->group(function () {

    Route::get('/dataKpr', function (Request $request) {

        return response()->json(['data'=>Kpr::paginate(10),'message'=>'Success','statusCode'=>200], 200);

    })->name('dataKpr');


    Route::get('/dataKpr/{nrp}', function (Request $request,$nrp) {

        if (is_null(Kpr::where('nrp',$nrp)->first())) {
            return response()->json(['data'=>[],'message'=>'Failed , Data tidak ditemukan :(','statusCode'=>404], 404);
        } else {
            return response()->json(['data'=>Kpr::where('nrp',$nrp)->first(),'message'=>'Success','statusCode'=>200], 200);
        }

    })->name('specificData');

    Route::get('/dataTransaksi/{nrp}', function (Request $request,$nrp) {

        if (is_null(Transaksi::where('nrp',$nrp)->first())) {
            return response()->json(['data'=>[],'message'=>'Failed , Data tidak ditemukan :(','statusCode'=>404], 404);
        } else {
            return response()->json(['data'=>Transaksi::where('nrp',$nrp)->first(),'message'=>'Success','statusCode'=>200], 200);
        }
    })->name('specificTransaksi');


    Route::get('/dataTransaksi', function (Request $request) {


            return response()->json(['data'=>Transaksi::paginate(10),'message'=>'Success','statusCode'=>200], 200);

    })->name('dataTransaksi');
});


Route::prefix('baltab')->group(function () {

    Route::get('/dataPokok', function (Request $request) {

        return response()->json(['data'=>Datapokok::paginate(10),'message'=>'Success','statusCode'=>200], 200);

    })->name('dataKpr');


    Route::get('/getTabungan/{nrp}', function (Request $request,$nrp) {

        if (is_null(Datapokok::where('nrp',$nrp)->first())) {
            return response()->json(['data'=>[],'message'=>'Failed , Data tidak ditemukan :(','statusCode'=>404], 404);
        } else {
            $prajurit =Datapokok::where('nrp',$nrp)->first();
            $hasil = app('App\Http\Controllers\rumusController')->cariTab($nrp);

            return response()->json(['message'=>'Data tabugan','message'=>'Success','statusCode'=>200,'prajurit'=>$prajurit,'tabungan'=>$hasil], 200);
        }

    })->name('dataTabungan');

    Route::get('/datapokokPembayaran', function () {

        $data = Datapokokpembayaran::where('status',3)->orderBy('created_at','DESC')->paginate(100);
        return response()->json(['data'=>$data,'message'=>'Success','statusCode'=>200], 200);

    })->name('datapokokPembayaran');

    
 

    Route::get('/cairkan', function (Request $request) {

        $req = json_decode($request->getContent(), true);
        if (is_null($req['datapokok_ids'])) {
            return response()->json(['message'=>'Anda tidak memilih 1 pengajuan pun'], 200);
        }
        $errors = [];
        $succes = [];

        foreach ($req['datapokok_ids'] as $key => $value) {
           try {
            Datapokokpembayaran::where('datapokok_id',$value)->update(
                [
                    'status'=>4
                ]
                );
                $succes[] = "Sukses merubah status di datapokok id ".$value;
           } catch (\Throwable $th) {
            $err[] = "terjadi error di datapokok id ".$value;
           }
        }


        return response()->json(['err'=>$err,'succ'=>$succ,'message'=>'Success','statusCode'=>200], 200);



    })->name('cairkan');


    Route::get('/datapokokCair', function () {

        $data = Datapokokpembayaran::where('status',4)->orderBy('created_at','DESC')->paginate(100);
        return response()->json(['data'=>$data,'message'=>'Success','statusCode'=>200], 200);

    })->name('datapokokCair');

    
   

});


});



// url = baltab/api/dataPokok
// type = GET
// param = token (specToken)


// url = baltab/api/getData/{nrp}
// type = GET
// param = token (specToken) ,nrp
// ex = api/getTab/1234567

// url = baltab/api/pembayaran
// type = GET
// param = token (specToken)
