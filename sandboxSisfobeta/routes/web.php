<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Models\Datapokok;
use App\Models\User;
use App\Models\Kpr;
use App\Models\Transaksi;
// use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/getToken', function () {
    return view('getToken');
})->name('getToken');


Route::post('/generateToken', function () {
    $token = Str::substr(Crypt::encrypt(now()), 0, 10);
    DB::table('token')->insert([
        'user_id'=>Auth::id(),
        'token'=>$token,
        'group_id'=>0
    ]);

    return redirect()->back()->with(['message'=>'token sandbox telah dibuat']);

})->name('generateToken');


Route::get('/account', function () {
    $data = Datapokok::where('user_id',Auth::id())->first();
    return view('profile',['data'=>$data]);
})->name('account');



Route::post('/updateData', function (Request $request) {
    User::where('id',Auth::id())->update(['name'=>$request->nama,'narek'=>$request->narek,'nabank'=>$request->nabank,'norek'=>$request->norek]);
    Datapokok::where('user_id',Auth::id())->update($request->except(['_token']));
    return redirect()->back()->with(['message'=>'Datapokok dan Acc telah diperbarui']);

})->name('updateData');
Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('kpr')->name('kpr.')->group(function () {
    
Route::get('/', function ()
{
    $rumah = DB::table('rumah')->get();
   
    if (is_null($kpr = DB::table('kpr')->where('user_id',Auth::id())->first())) {
        return view('kpr.index',['rumah'=>$rumah]);
    } else {
        

        $kpr = Kpr::where('user_id',Auth::id())->first();
        $waktu_selesai = date('Y-m-d', strtotime("+".$kpr->jk_waktu." months", strtotime($kpr->tmt_angsuran)));
        $tahun_selesai = explode('-',$waktu_selesai);

        $besar_pinjaman = $kpr->pinjaman;
        $bunga = 6/100;
        $jangka = $kpr->jk_waktu;
        $tahun = $kpr->jk_waktu/12;

        $c = pow((1 + $bunga), $tahun);
        $d = $c - 1;
        $fax = ($bunga * $c) / $d;

        //tergantung mau dibuletinnya gimana
        $anunitas = round($fax, 6);
        $besar_angsur = ($besar_pinjaman * $anunitas) / 12;
        $besar_angsuran = round($besar_angsur, -3) + 1000;


        $transaksi = Transaksi::where('nrp', Auth::user()->nrp)
        ->where('status', '0001')
        ->get();

        // dd($transaksi);
        $tahun_awal = date('Y', strtotime($kpr->tmt_angsuran));
        $years = [];
        for ($i = $tahun_awal; $i <= $tahun_selesai[0]; $i++) {
            $years[] = $i;
        }

        $years = array_values(array_reverse(array_map('strval', $years), true));
        return view('kpr.index',['rumah'=>$rumah,'kpr'=>$kpr,'transaksi'=>$transaksi,'years'=>$years,'besar_angsuran'=>$besar_angsuran]);
    }
    

})->name('index');

Route::post('/bayarPost', [App\Http\Controllers\KprController::class, 'bayarPost'])->name('bayarPost');




Route::post('/pinjaman', [App\Http\Controllers\KprController::class, 'pinjaman'])->name('pinjaman');
   


Route::get('/refreshData', [App\Http\Controllers\KprController::class, 'refreshData'])->name('refreshData');
    

Route::get('/deleteKpr', function ()
{
    Kpr::where('user_id',Auth::id())->delete();
    Transaksi::where('nrp',Auth::user()->nrp)->delete();
    return redirect()->back()->with(['message'=>'berhasil di delete']);


    

})->name('deleteKpr');



});

Route::prefix('baltab')->name('baltab.')->group(function () {
    
    Route::get('/', function ()
    {
        return view('baltab.index');
    })->name('index');
    
    });



Route::prefix('twp')->name('twp.')->group(function () {
    
    Route::get('/', function ()
    {
        return view('twp.index');
    })->name('index');
    
    });

    Route::prefix('api')->name('api.')->group(function () {
    
        Route::get('/baltab', function ()
        {
            return view('api.baltab');
        })->name('baltab');

        Route::get('/kpr', function ()
        {
            return view('api.kpr');
        })->name('kpr');

        Route::get('/etwp', function ()
        {
            return view('api.etwp');
        })->name('etwp');
        
        Route::get('/sitp', function ()
        {
            return view('api.sitp');
        })->name('sitp');
        
        });

        

